#
#    _/                _/        _/
# _/_/_/_/    _/_/_/  _/_/_/    _/    _/_/      _/_/_/    _/_/    _/_/_/
#  _/      _/    _/  _/    _/  _/  _/_/_/_/  _/    _/  _/_/_/_/  _/    _/
# _/      _/    _/  _/    _/  _/  _/        _/    _/  _/        _/    _/
#  _/_/    _/_/_/  _/_/_/    _/    _/_/_/    _/_/_/    _/_/_/  _/    _/
#                                               _/
#                                           _/_/

import os
import argparse

import diag_printer


class NormalizeAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, os.path.normpath(values))


argp = argparse.ArgumentParser(description='''
        Parses token definition files and builds the corresponding tables''')

argp.add_argument("file", metavar="<definitions file>", type=argparse.FileType(),
                  help="the file from which the definitions are to be read. Pass '-' for STDIN.")
argp.add_argument("-T", "--templates", default="templates", metavar="<dir>",
                  help="path to template directory")
argp.add_argument("-o", "--output-dir", dest="outdir", metavar="<dir>", default='out',
                  help="path to the output directory", action=NormalizeAction)
argp.add_argument("-v", "--verbose", action="store_true",
                  help="print additional output")
argp.add_argument("--print-tree", dest="print_tree", action="store_true",
                  help="print the parsed trees, [default if --parse-only]")
argp.add_argument("--no-tree", dest="no_tree", action="store_false",
                  help="do not print the parsed tree, although --parse-only is given")
argp.add_argument("--parse-only", dest="parse_only", action="store_true",
                  help="do not generate the tables, only parse the input")
argp.add_argument("--print-struct", dest="print_struct", action="store_true",
                  help="print the structure which is passed to the template engine")

args = argp.parse_args()

import yacc
import model
import itertools
import codecs

yacc.create_parser(file=args.file.name)
result, error = yacc.do_parsing(args.file.read())

if error:
    exit(1)

if args.print_tree or (args.parse_only and args.no_tree is None):
    print "PARSED TREE\n======"
    model.pretty_print(result)

if args.parse_only:
    exit(0)

if not os.path.exists(args.outdir):
    os.makedirs(args.outdir)

from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader(args.templates))
struct = model.build_structures(result, diag_printer.DiagPrinter())

if args.print_struct:
    model.pretty_print(struct)

for file in ["TokenTable.h"]:
    template = env.get_template(file)
    output = template.render(struct)

    with codecs.open('/'.join([args.outdir, file]), mode='w', encoding='utf-8') as outfile:
        outfile.write(output)

#
#    _/                _/        _/
# _/_/_/_/    _/_/_/  _/_/_/    _/    _/_/      _/_/_/    _/_/    _/_/_/
#  _/      _/    _/  _/    _/  _/  _/_/_/_/  _/    _/  _/_/_/_/  _/    _/
# _/      _/    _/  _/    _/  _/  _/        _/    _/  _/        _/    _/
#  _/_/    _/_/_/  _/_/_/    _/    _/_/_/    _/_/_/    _/_/_/  _/    _/
#                                               _/
#                                           _/_/

import os
import sys


class Location(object):
    def __init__(self, file, line=None, column=None):
        self.file = file
        self.line = line
        self.column = column

    def __repr__(self):
        return '<Location %s>' % str(self)

    def __str__(self):
        return ':'.join([self.file or '', self.line or '', self.column or ''])


class DiagPrinter(object):
    def __init__(self, stream=sys.stderr, base=None):
        self.base = base or os.getcwd()
        self.stream = stream

    def message(self, loc, type, msg, fmt):
        if len(fmt) > 0: msg %= tuple(fmt)
        print >>self.stream, ':'.join([str(loc), type, ' ' + msg])

    def info(self, loc, msg, *fmt):
        self.message(loc, 'info', msg, fmt)

    def warning(self, loc, msg, *fmt):
        self.message(loc, 'warning', msg, fmt)

    def error(self, loc, msg, *fmt):
        self.message(loc, 'error', msg, fmt)

#
#    _/                _/        _/
# _/_/_/_/    _/_/_/  _/_/_/    _/    _/_/      _/_/_/    _/_/    _/_/_/
#  _/      _/    _/  _/    _/  _/  _/_/_/_/  _/    _/  _/_/_/_/  _/    _/
# _/      _/    _/  _/    _/  _/  _/        _/    _/  _/        _/    _/
#  _/_/    _/_/_/  _/_/_/    _/    _/_/_/    _/_/_/    _/_/_/  _/    _/
#                                               _/
#                                           _/_/

import sys
import ply.yacc as yacc

import lex
from lex import tokens
from model import *
from matcher import *
import model

parser = None

def p_start(p):
    '''definitions_file : group_decl_list'''
    p[0] = p[1]

def p_group_decl_list(p):
    '''group_decl_list : group_decl_list group_decl'''
    p[0] = p[1] + [p[2]]

def p_group_decl_list_empty(p):
    '''group_decl_list : '''
    p[0] = []

def p_group_decl(p):
    '''group_decl : iden_pair '{' group_child_list '}' '''
    p[0] = Group(*p[1], children=p[3])

def p_iden_pair(p):
    '''iden_pair : user_name'''
    name, lineno = p[1]
    p[0] = (name, None), lineno

def p_extended_iden_pair(p):
    '''iden_pair : user_name ':' LC_NAME'''
    name, lineno = p[1]
    p[0] = (name, p[3]), lineno

def p_user_name_rec(p):
    '''user_name : user_name NAME_PART'''
    name, lineno = p[1]
    p[0] = ' '.join([name, p[2]]), lineno

def p_user_name(p):
    '''user_name : NAME_PART'''
    p[0] = p[1], p.lineno(1)

def p_group_child_list_rec(p):
    '''group_child_list : group_child_list ',' group_child'''
    p[0] = p[1] + [p[3]]

def p_group_child_list_empty(p):
    '''group_child_list : group_child'''
    p[0] = [p[1]]

def p_group_child(p):
    '''group_child : iden_pair '=' matcher'''
    p[0] = Token(*p[1], matcher=p[3])

def p_simple_group_child(p):
    '''group_child : iden_pair'''
    name, line = p[1]
    p[0] = Token(name, line, CompareMatcher(name[0]))

def p_regex_matcher(p):
    '''matcher : PAT_REG'''
    p[0] = RegexMatcher(p[1][1:-1])

def p_raw_matcher(p):
    '''matcher : PAT_RAW'''
    p[0] = CompareMatcher(p[1][2:-1])

def p_error(p):
    parser.print_error(p, "syntax error at token %s (%s)" % (p.type, p.value))
    parser.has_error = True
    yacc.errok()

def create_parser(file='<stdin>', errfile=sys.stderr):
    global parser

    def print_error(p, msg):
        last_cr = p.lexer.lexdata.rfind('\n', 0, p.lexpos)
        column = p.lexpos - (last_cr if last_cr > 0 else 0) + 1
        errfile.write('error:%s:%d:%d: %s\n' % (file, p.lineno, column, msg))

    lex.create_lexer(file, errfile)
    parser = yacc.yacc(write_tables=0, debug=0)
    parser.has_error = False
    parser.print_error = print_error
    return parser

def do_parsing(input, lexer=lex.lexer):
    # global parser
    tree = yacc.parse(input, lexer=lexer)
    return (tree, False) #parser.has_error or lexer.has_error)

if __name__ == '__main__':
    if len(sys.argv) != 2 or sys.argv[1] in ['-h', '--help']:
        print "usage: %s <input-file>" % sys.argv[0]
        exit(len(sys.argv) * -1 + 2) # exit code 0 on --help else 1

    with open(sys.argv[1]) as infile:
        inp = infile.read()

    create_parser(sys.argv[1])
    result, error = do_parsing(inp)

    if error:
        exit(1)

    print "RESULT\n======"
    pretty_print(result)

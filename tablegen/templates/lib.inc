//
//  lib.inc
//  ti82/tablegen
//
//  Created by Janek Spaderna on 21.01.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

{% macro tok(t, parens=true) -%}{{ [t.group.short, t.c]|join('_') }}{{ '()' if parens }}{% endmacro %}

{% macro table(all, type, name, args=[], pre=None, default="TABLEGEN_UNREACHABLE();") -%}
{{ type|indent(indentfirst=true) }} {{ name }}({{ args|join(', ') }}) const {
{{ (pre + ';')|indent(8, true) if pre is not none }}
        switch (val) {
{% for t in all %}
        case {{ loop.index }}: /* {{ tok(t, false) }} */
            {{ caller(t)|trim }}{% endfor %}
        default: {{ default }}
        }
    }
{%- endmacro %}

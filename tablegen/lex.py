#
#    _/                _/        _/
# _/_/_/_/    _/_/_/  _/_/_/    _/    _/_/      _/_/_/    _/_/    _/_/_/
#  _/      _/    _/  _/    _/  _/  _/_/_/_/  _/    _/  _/_/_/_/  _/    _/
# _/      _/    _/  _/    _/  _/  _/        _/    _/  _/        _/    _/
#  _/_/    _/_/_/  _/_/_/    _/    _/_/_/    _/_/_/    _/_/_/  _/    _/
#                                               _/
#                                           _/_/

import sys
import ply.lex as lex

tokens = ['LC_NAME', 'NAME_PART', 'PAT_REG', 'PAT_RAW']
literals = '{}?=:,'
lexer = None

t_LC_NAME    = r'[a-z][a-zA-Z]*'
t_NAME_PART  = r'[A-Z][a-zA-Z]*'
t_PAT_REG    = r"'[^']+'"

def t_PAT_RAW(t):
    r"r'[^']+'"
    return t

def t_COMMENT(t):
    r'//.*'
    pass

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

t_ignore = ' \t'

def t_error(t):
    t.lexer.has_error = True
    t.lexer.print_error(t, 'unexpected character ' + t.value[0])
    t.lexer.skip(1)

def create_lexer(file='<stdin>', errfile=sys.stderr):
    global lexer

    def print_error(tok, msg):
        last_cr = tok.lexer.lexdata.rfind('\n', 0, tok.lexpos)
        column = tok.lexpos - (last_cr if last_cr > 0 else 0) + 1
        errfile.write(
                'error:%s:%d:%d: %s\n' % (file, lexer.lineno, column, msg))

    lexer = lex.lex()
    lexer.print_error = print_error
    lexer.has_error = False
    return lexer


if __name__ == '__main__':
    lex.runmain(create_lexer())

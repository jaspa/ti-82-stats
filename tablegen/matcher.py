
class Matcher(object):
    def __init__(self, pattern, wants_src=False, name=None):
        self.pattern = unicode(pattern, 'utf-8')
        self.name = self.__class__.__name__ if name is None else name
        self.wants_src = wants_src

    def escaped_pattern(self):
        return self.pattern.replace('\\', '\\\\').replace('"', '\\"')

    def creation_args(self):
        return ["pattern()", "src"] if self.wants_src else ["pattern()"]


class RegexMatcher(Matcher):
    def __init__(self, pattern):
        super(RegexMatcher, self).__init__(pattern, wants_src=True)


class CompareMatcher(Matcher):
    def __init__(self, pattern):
        super(CompareMatcher, self).__init__(pattern)

#
# _/                _/        _/
# _/_/_/_/    _/_/_/  _/_/_/    _/    _/_/      _/_/_/    _/_/    _/_/_/
#  _/      _/    _/  _/    _/  _/  _/_/_/_/  _/    _/  _/_/_/_/  _/    _/
# _/      _/    _/  _/    _/  _/  _/        _/    _/  _/        _/    _/
#  _/_/    _/_/_/  _/_/_/    _/    _/_/_/    _/_/_/    _/_/_/  _/    _/
#                                               _/
#                                           _/_/


def _lowerFirst(s):
    return s[0].lower() + s[1:]


def _apply(func, arg, apply=True):
    return func(arg) if apply and func is not None else arg


def _make_c(s, lower):
    return _apply(_lowerFirst, ''.join(s.split()), lower)


class NamedObject(object):
    def __init__(self, name, line, lowerC):
        self.line = line
        self.user, self.c = name

        if self.c is None:
            self.c = _make_c(self.user, lowerC)

    def __repr__(self):
        return '%s @%d' % (self.user, self.line)


class Token(NamedObject):
    def __init__(self, name, line, matcher):
        super(Token, self).__init__(name, line, lowerC=True)
        self.matcher = matcher
        self.line = line
        self.group = None


class Group(NamedObject):
    def __init__(self, name, line, children):
        user, c = name
        super(Group, self).__init__((user, None), line, lowerC=False)
        self.long = _make_c(user, lower=False)
        self.short = _lowerFirst(self.long) if c is None else c
        self.children = children

        for tok in children:
            tok.group = self

    def __iter__(self):
        return iter(self.children)


# [Group] -> {groups: [Group], tokens: [Token]}
def build_structures(groups, diags):
    group_names = {}
    tok_names = {}

    all_tokens = []

    for group in groups:
        try:
            line = group_names[group.c]
            diags.warning(group.line, "Group '%s' already declared in line %d. Extending.", group.c, line)
        except KeyError:
            group_names[group.c] = group.line

        for token in group:
            try:
                line = tok_names[group.c + ':' + token.c]
                diags.error(token.line, "Token '%s' in Group '%s' already declared in line %d.", token.c, group.c, line)
            except KeyError:
                tok_names[group.c + ':' + token.c] = token.line

            all_tokens.append(token)

    return dict(groups=groups, tokens=all_tokens)


def pretty_print(tree, **kwargs):
    from pprint import pprint

    pprint(tree, **kwargs)

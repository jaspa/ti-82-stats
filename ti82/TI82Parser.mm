//
//  TI82Parser.m
//  TI-82 STATS
//
//  Created by Janek Spaderna on 16.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82Parser.h"
#import "TI82Tokenizer.h"
#import "Functions.h"

#include <llvm/ADT/SmallVector.h>
#include <vector>

using namespace AST;

#define TI82_ERROR(Src, Fmt, ...)                                                     \
    do {                                                                              \
        Diagnosis::send(diagnosisHandler(), Diagnosis::Error, [=](Diagnosis & diag) { \
            diag.errorRange.range = (Src);                                            \
            diag.errorRange.base = &Tokens.sourceHandler();                           \
            diag.message = [NSString stringWithFormat:(Fmt), ##__VA_ARGS__];          \
        });                                                                           \
    } while (0)

#define TI82_ERROR2(Src, Notes, Fmt, ...)                                             \
    do {                                                                              \
        Diagnosis::send(diagnosisHandler(), Diagnosis::Error, [=](Diagnosis & diag) { \
            diag.errorRange.range = (Src);                                            \
            diag.errorRange.base = &Tokens.sourceHandler();                           \
            diag.message = [NSString stringWithFormat:(Fmt), ##__VA_ARGS__];          \
            diag.notes = (Notes);                                                     \
        });                                                                           \
    } while (0)


namespace TI82 {
namespace {

class ParserImpl {
    Tokenizer Tokens;
    __weak id<DiagnosisHandler> _DiagHandler;
    id<DiagnosisHandler> diagnosisHandler() { return _DiagHandler; }

    struct {
        bool complexLiterals = true;
        bool tolerateErrors = false;
    } state;

    static std::pair<precedence_t, bool> lookupBinOP(MatchedToken tok) {
        if (precedence_t prec = lookupInfixFunction(tok))
            return {prec, false};
        return {lookupInfixFunction(Tok::op_multiply()), true};
    }

    const char16_t *checkAndAdvance(Tok check) {
        auto cur = Tokens.getCurrentToken();

        if (cur != check)
            return cur.source().ptr();

        Tokens.getNextToken();
        return cur.source().ptr_end();
    }

    ASTBlockStatement *statementBlock(MatchedToken &endTok) {
        NSMutableArray *nodes = [NSMutableArray arrayWithCapacity:16];

        while (true) {
            auto tok = Tokens.getCurrentToken();

            if ((endTok != Tok() && endTok == tok) || tok == Tok::ctrl_end()) {
                endTok = tok;
                break;
            }

            auto next = node();

            if (!next)
                return nil;

            [nodes addObject:next];
        }

        auto end = Tokens.getNextToken();

        if (end.group() != TokGroup::EndOfStatement()) {
            TI82_ERROR(end.source(), @"unexpected token – expected EndOfStatement");
            return nil;
        }

        return [ASTBlockStatement blockWithNodes:nodes source:SourceRef()];
    }

    ASTExpression *conditionHeader() {
        if (auto cond = expression()) {
            auto tok = Tokens.getCurrentToken();

            if (tok.group() == TokGroup::EndOfStatement()) {
                Tokens.getNextToken();
                return cond;
            }

            TI82_ERROR(tok.source(), @"unexpected token – expected EndOfStatement");
        }

        return nil;
    }

    ASTIfStatement *ifStatement() {
        auto ifTok = Tokens.getCurrentToken();
        NSCParameterAssert(ifTok == Tok::ctrl_if());

        Tokens.getNextToken();
        auto condition = conditionHeader();

        if (!condition)
            return nil;

        if (Tokens.getCurrentToken() != Tok::ctrl_then()) {
            if (auto body = node()) {
                auto block = [ASTBlockStatement blockWithNodes:@[ body ] source:body.source];
                return [ASTIfStatement if:condition then:block else:nil source:ifTok.source()];
            }

            return nil;
        }

        auto tok = Tokens.getNextToken();    // Consume the 'then'
        if (tok.group() != TokGroup::EndOfStatement()) {
            TI82_ERROR(tok.source(), @"unexpected token – expected EndOfStatement");
            return nil;
        }

        Tokens.getNextToken();    // Consume the EOS

        tok = MatchedToken(Tok::ctrl_else(), llvm::None);
        auto thenBlock = statementBlock(tok);

        if (!thenBlock)
            return nil;

        if (tok == Tok::ctrl_end())
            return [ASTIfStatement if:condition then:thenBlock else:nil source:ifTok.source()];

        tok = MatchedToken();
        auto elseBlock = statementBlock(tok);

        if (!elseBlock)
            return nil;

        return [ASTIfStatement if:condition then:thenBlock else:elseBlock source:ifTok.source()];
    }

    ASTConditionedLoopStatement *simpleLoop() {
        auto loopTok = Tokens.getCurrentToken();
        NSCParameterAssert(loopTok == Tok::ctrl_while() || loopTok == Tok::ctrl_repeat());

        Tokens.getNextToken();
        ASTExpression *condition = expression();

        auto tok = Tokens.getCurrentToken();
        if (tok.group() != TokGroup::EndOfStatement()) {
            TI82_ERROR(tok.source(), @"unexpected token – expected EndOfStatement");
            return nil;
        }

        tok = MatchedToken();
        if (auto body = statementBlock(tok)) {
            return [NSClassFromString([NSString stringWithFormat:@"AST%sLoop", loopTok.name()])
                loopOverBlock:body
                    condition:condition
                       source:loopTok.source()];
        }

        return nil;
    }

    ASTForLoop *forLoop() {
        auto forTok = Tokens.getCurrentToken();
        NSCParameterAssert(forTok == Tok::ctrl_for());

        auto Tok = Tokens.getNextToken();
        if (Tok != Tok::var_num()) {
            TI82_ERROR(Tok.source(), @"invalid variable – expected a number variable");
            return nil;
        }

        ASTNumberVarRef *idxVar = [ASTNumberVarRef varRefWithName:Tok.source()];

        if ((Tok = Tokens.getNextToken()) != Tok::p_comma()) {
            TI82_ERROR(Tok.source(), @"unexpected token – expected ','");
            return nil;
        }

        Tokens.getNextToken();    // consume ','
        NSArray *exprs = commaSeparatedList(Tok::p_rparen(), false, 3);

        if (!exprs)
            return nil;

        Tok = Tokens.getCurrentToken();

        if (exprs.count < 2) {
            TI82_ERROR(Tok.source(), @"unexpected token – For( head terminated to early");
            return nil;
        }

        if (Tok == Tok::p_rparen())
            Tok = Tokens.getNextToken();

        if (Tok.group() != TokGroup::EndOfStatement()) {
            TI82_ERROR(Tok.source(), @"unexpected token – expected ')' or EndOfStatement");
            return nil;
        }

        Tok = MatchedToken();
        if (ASTBlockStatement *body = statementBlock(Tok)) {
            return [ASTForLoop loopFor:idxVar
                                  from:exprs[0]
                                    to:exprs[1]
                                    by:exprs.count > 2 ? exprs[2] : nil
                                    do:body
                                source:forTok.source()];
        }

        return nil;
    }

    ASTDisplayStmt *displayStatement() {
        auto displayTok = Tokens.getCurrentToken();
        NSCParameterAssert(displayTok == Tok::io_disp());

        NSMutableArray *toDisplay = [NSMutableArray array];

        while (Tokens.getNextToken().group() != TokGroup::EndOfStatement()) {
            ASTExpression *expr = expression();

            if (!expr)
                return nil;

            [toDisplay addObject:expr];

            auto Tok = Tokens.getCurrentToken();
            if (Tok.group() == TokGroup::EndOfStatement())
                break;

            if (Tokens.getCurrentToken() != Tok::p_comma()) {
                TI82_ERROR(Tokens.getCurrentToken().source(), @"unexpected token – expected ',' or EndOfStatement");
                return nil;
            }
        }

        return [ASTDisplayStmt
            displayExpressions:toDisplay
                        source:{displayTok.source().ptr(), [(ASTNode *)toDisplay.lastObject source].ptr_end()}];
    }

    std::pair<ASTStatement *, bool> statement() {
        auto Tok = Tokens.getCurrentToken();

        if (Tok == Tok::io_disp())
            return {displayStatement(), true};

        if (Tok == Tok::ctrl_if())
            return {ifStatement(), true};

        if (Tok == Tok::ctrl_while() || Tok == Tok::ctrl_repeat())
            return {simpleLoop(), true};

        if (Tok == Tok::ctrl_for())
            return {forLoop(), true};

        return {nil, false};
    }

    ASTNode *statementSuffix(ASTExpression *expr) {
        auto Tok = Tokens.getCurrentToken();

        if (Tok == Tok::mod_arrow()) {
            if ((Tok = Tokens.getNextToken()).group() != TokGroup::VariableReference())
                TI82_ERROR(Tok.source(), @"unexpected token – expected: Variable Reference");

            else if (Tok == Tok::var_ans())
                TI82_ERROR(Tok.source(), @"invalid destination – cannot store into Ans");

            else if (ASTVarRef *dest = variableReference()) {
                return [ASTStoreStmt storeExpression:expr
                                        intoVariable:dest
                                              source:{expr.source.ptr(), dest.source.ptr_end()}];
            }

            return nil;
        }

        TI82_ERROR(Tok.source(), @"unexpected token – expected: StoreArrow or EndOfStatement");
        return nil;
    }

    ASTExpression *expression() {
        if (ASTExpression *expr = operand())
            if ((expr = binaryOperationRHS(expr, 0)))
                return postfixOperation(expr);
        return nil;
    }

    ASTFunctionCall *functionCall() {
        auto ID = Tokens.getCurrentToken();
        Tokens.getNextToken();

        if (NSArray *args = commaSeparatedList(Tok::p_rparen())) {
            return [ASTFunctionCall functionCallWithID:ID
                                             arguments:args
                                                source:{ID.source().ptr(), checkAndAdvance(Tok::p_rparen())}];
        }

        return nil;
    }

    ASTExpression *operand(precedence_t prec = 0) {
        ASTExpression *expr;
        auto Tok = Tokens.getCurrentToken();

        if (precedence_t prec2 = lookupPrefixFunction(Tok)) {
            Tokens.getNextToken();

            if ((expr = operand(prec)) && (expr = binaryOperationRHS(expr, prec2))) {
                return [ASTFunctionCall functionCallWithID:Tok
                                                 arguments:@[ expr ]
                                                    source:{Tok.source().ptr(), expr.source.ptr_end()}];
            }

            return nil;
        }

        if (Tok == Tok::p_lparen())
            expr = parentheses();

        else if (Tok == Tok::lit_strDel())
            expr = stringLiteral();

        else if (Tok == Tok::lit_lstStart()) {
            if (!state.complexLiterals) {
                TI82_ERROR2(Tok.source(), @[ @"(complex literals in this context not allowed)" ],
                            @"unexpected token – expected expression");
                return nil;
            }

            expr = listLiteral();
        }

        else if (Tok == Tok::lit_num())
            expr = numberLiteral();

        else if (checkCallFunction(Tok))
            expr = functionCall();

        else if (Tok.group() == TokGroup::VariableReference())
            expr = variableReference();

        else if (!state.tolerateErrors)
            TI82_ERROR(Tok.source(), @"unexpected token – expected expression");

        return expr ? postfixOperation(expr, prec) : nil;
    }

    ASTExpression *binaryOperationRHS(ASTExpression *LHS, precedence_t prec = 0) {
        while (true) {
            auto Tok = Tokens.getCurrentToken();

            bool implicitMul;
            precedence_t funPrec;
            std::tie(funPrec, implicitMul) = lookupBinOP(Tok);

            if (funPrec <= prec)
                return LHS;

            if (!implicitMul) {
                Tokens.getNextToken();
            }

            std::swap(implicitMul, state.tolerateErrors);
            ASTExpression *RHS = operand(funPrec);
            std::swap(implicitMul, state.tolerateErrors);

            if (!RHS) {
                return implicitMul ? LHS : nil;
            }

            precedence_t NextPrec = lookupBinOP(Tokens.getCurrentToken()).first;
            if (funPrec < NextPrec) {
                RHS = binaryOperationRHS(RHS, funPrec);

                if (!RHS)
                    return nil;
            }

            auto LHSSource = LHS.source, RHSSource = RHS.source;

            LHS = [ASTFunctionCall functionCallWithID:Tok
                                            arguments:@[ LHS, RHS ]
                                               source:{LHSSource.ptr(), RHSSource.ptr_end()}];
        }
    }

    ASTExpression *postfixOperation(ASTExpression *operand, precedence_t prec = 0) {
        auto Tok = Tokens.getCurrentToken();

        if (checkPostfixFunction(Tok)) {
            Tokens.getNextToken();
            return [ASTFunctionCall functionCallWithID:Tok
                                             arguments:@[ operand ]
                                                source:{operand.source.ptr(), Tok.source().ptr_end()}];
        }

        return operand;
    }

    ASTExpression *parentheses() {
        NSCParameterAssert(Tokens.getCurrentToken() == Tok::p_lparen());

        Tokens.getNextToken();    // Consume '('
        ASTExpression *Expr = expression();

        if (!Expr)
            return nil;

        if (Tokens.getCurrentToken() == Tok::p_rparen())
            Tokens.getNextToken();    // Consume ')'

        return Expr;
    }

    NSArray *commaSeparatedList(Tok End, bool allowArrow = true, NSUInteger max = NSUIntegerMax) {
        NSMutableArray *elements = [NSMutableArray arrayWithCapacity:8];

        while (ASTExpression *expr = expression()) {
            [elements addObject:expr];

            auto Tok = Tokens.getCurrentToken();

            if (Tok == End || Tok.group() == TokGroup::EndOfStatement() || (allowArrow && Tok == Tok::mod_arrow()) ||
                elements.count == max)
                return elements;

            if (Tok != Tok::p_comma()) {
                TI82_ERROR(Tok.source(), @"unexpected token – expected ',', %s%s or EndOfStatement", End.name(),
                           allowArrow ? ", StoreArrow" : "");
                return nil;
            }

            Tokens.getNextToken();
        }

        return nil;
    }

    ASTStringLiteral *stringLiteral() {
        auto Start = Tokens.getCurrentToken();
        NSCParameterAssert(Start == Tok::lit_strDel());

        MatchedToken End;
        llvm::SmallVector<uint32_t, 8> userIndices;
        userIndices.push_back(0);

        while (End != Tok::lit_strDel() && End != Tok::mod_arrow() && End != Tok::eos_newline()) {
            if (!(End = Tokens.getNextToken([](Tok t) { return t != Tok::lit_num(); })))
                return nil;

            userIndices.push_back(userIndices.back() + static_cast<uint32_t>(End.source().size()));
        }

        userIndices.pop_back();
        return
            [ASTStringLiteral stringLiteralWithUserIndices:userIndices
                                                    source:{Start.source().ptr(), checkAndAdvance(Tok::lit_strDel())}];
    }

    ASTListLiteral *listLiteral() {
        auto Start = Tokens.getCurrentToken();
        NSCParameterAssert(Start == Tok::lit_lstStart());

        bool complexBefore = false;
        std::swap(complexBefore, state.complexLiterals);
        at_scope_end = [=] { state.complexLiterals = complexBefore; };

        Tokens.getNextToken();    // Consume '{'

        NSArray *elements = commaSeparatedList(Tok::lit_lstEnd());
        return [ASTListLiteral listLiteralWithElements:elements
                                                source:{Start.source().ptr(), checkAndAdvance(Tok::lit_lstEnd())}];
    }

    ASTNumberLiteral *numberLiteral() {
        auto source = Tokens.getCurrentToken().source();
        NSString *numStr = [source.getNSString() stringByReplacingOccurrencesOfString:@"ᴇ" withString:@"e"];

        M_APM val = m_apm_init();
        m_apm_set_string(val, const_cast<char *>(numStr.UTF8String));

        Tokens.getNextToken();    // Consume the number
        return [ASTNumberLiteral numberLiteralWithValue:val source:source];
    }

    ASTVarRef *variableReference() {
        auto Tok = Tokens.getCurrentToken();
        NSCParameterAssert(Tok.group() == TokGroup::VariableReference());
        Tokens.getNextToken();
        return [NSClassFromString([NSString stringWithFormat:@"AST%sVarRef", Tok.name()]) varRefWithName:Tok.source()];
    }

public:
    ParserImpl(SourceHandler &FullSource, id<DiagnosisHandler> handler) : Tokens(FullSource), _DiagHandler(handler) {
        Tokens.TokenFallback = [this](NSUInteger &offset, SourceRef source) {
            return MatchedToken(Tok(), source.substr(NSRange{offset, 1}));
        };

        Tokens.getNextToken();
    }

    ASTNode *node() {
        auto Tok = Tokens.getCurrentToken();

        if (!Tok) {
            Tok[0] >> [this](SourceRef source) { TI82_ERROR(source, @"unexpected character in top level statement"); };
            return nil;
        }

        if (Tok.group() == TokGroup::EndOfStatement()) {
            Tokens.getNextToken();
            return [ASTBlockStatement nodeWithSource:Tok.source()];
        }

        {
            auto stmtParseRes = statement();
            if (stmtParseRes.second) {
                if (stmtParseRes.first && (Tok = Tokens.getCurrentToken()).group() != TokGroup::EndOfStatement()) {
                    TI82_ERROR(Tok.source(), @"unexpected token – expected: EndOfStatement");
                    return nil;
                }

                return stmtParseRes.first;
            }
        }

        ASTNode *Result = expression();

        if (!Result)
            return nil;

        if (Tokens.getCurrentToken().group() != TokGroup::EndOfStatement()) {
            Result = statementSuffix((ASTExpression *)Result);

            if ((Tok = Tokens.getCurrentToken()).group() != TokGroup::EndOfStatement()) {
                TI82_ERROR(Tok.source(), @"unexpected token – expected: EndOfStatement");
                return nil;
            }
        }

        Tokens.getNextToken();
        return Result;
    }

    void skipToken() { Tokens.getNextToken(); }
    bool completed() { return Tokens.finished(); }
};
}

Parser::Parser(SourceHandler &FullSource, id<DiagnosisHandler> DiagHandler)
    : impl(new ParserImpl(FullSource, DiagHandler)) {
}

Parser::~Parser() {
    delete static_cast<ParserImpl *>(impl);
}

ASTNode *Parser::ParseNode() {
    return static_cast<ParserImpl *>(impl)->node();
}

void Parser::skipToken() {
    static_cast<ParserImpl *>(impl)->skipToken();
}

bool Parser::completed() const {
    return static_cast<ParserImpl *>(impl)->completed();
}
}

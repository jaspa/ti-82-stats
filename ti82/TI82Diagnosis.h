//
//  TI82Diagnosis.h
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <llvm/Support/raw_ostream.h>

#import "TI82SourceRef.h"
#import "TI82SourceHandler.h"

@protocol DiagnosisHandler;

namespace TI82 {

struct ErrorRange {
    SourceRef range;
    const char16_t *hotPoint;
    SourceHandler *base;

    ErrorRange() : range(), hotPoint(nullptr), base(nullptr) {}
    ErrorRange(SourceHandler *src, SourceRef range, const char16_t *point = nullptr)
        : range(range), hotPoint(point), base(src) {}
};

struct Diagnosis {
    enum Kind : uint8_t { Notice, Warning, Error };

    Kind kind;
    ErrorRange errorRange;
    NSString *message;
    NSArray *notes;

    template <class UnaryFunc>
    static void send(id<DiagnosisHandler> receiver, Kind kind, UnaryFunc builder) {
        Diagnosis diag;
        diag.kind = kind;
        builder(diag);
        std::move(diag).deliver(receiver);
    }

    void render(llvm::raw_ostream &os) const;
    void deliver(id<DiagnosisHandler> receiver) && ;
};
}

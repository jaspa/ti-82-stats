//
//  FunctionRegistry.h
//  ti82
//
//  Created by Janek Spaderna on 16.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TI82AST.h"

@interface FunctionRegistry : NSObject

- (id)getFunction:(Tok)funcID;

@end

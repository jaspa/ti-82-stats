//
//  Types.h
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "Macros.h"
#import <functional>
#import <iterator>
#import <utility>
#import <Foundation/Foundation.h>

template <class T>
using Two = std::pair<T, T>;

namespace std {
template <>
class back_insert_iterator<NSMutableArray *> : public std::iterator<std::output_iterator_tag, void, void, void, void> {
public:
    typedef NSMutableArray *container_type;

protected:
    container_type container;

public:
    explicit back_insert_iterator(container_type array) : container(array) {}

    back_insert_iterator<container_type> &operator=(id object) {
        [container addObject:object];
        return *this;
    }

    back_insert_iterator<container_type> &operator*() { return *this; }
    back_insert_iterator<container_type> &operator++() { return *this; }
    back_insert_iterator<container_type> &operator++(int) { return *this; }
};
}

struct EndExec {
    template <class Func>
    EndExec(Func f)
        : f(f) {}

    ~EndExec() { f(); }

    EndExec(EndExec &&e) : f(std::move(e.f)) {}
    std::function<void()> f;
};

#define at_scope_end EndExec CON(__scope_end_, __LINE__)

//
//  FunctionRegistry.m
//  ti82
//
//  Created by Janek Spaderna on 16.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "FunctionRegistry.h"

@implementation FunctionRegistry {
    NSCache *cache;
}

- (id)getFunction:(Tok)funcID {
    if (id fun = [cache objectForKey:@(funcID.idx())]) {
        return fun;
    }

    SEL funSelector = sel_getUid(funcID.name());
    if (![self respondsToSelector:funSelector]) {
        printf("fatal error: function %s not implemented\n", funcID.name());
        exit(EXIT_FAILURE);
    }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    id fun = [self performSelector:funSelector];
#pragma clang diagnostic pop
    [cache setObject:fun forKey:@(funcID.idx())];
    return fun;
}

@end

//
//  UserRunner.h
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DiagnosisHandler.h"

@interface UserRunner : NSObject<DiagnosisHandler>

- (instancetype)initWithFile:(NSString *)file;

@property (nonatomic, readonly, copy) NSString *file;

- (int)run;

@end

//
//  main.mm
//  ti82
//
//  Created by Janek Spaderna on 01.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <llvm/Support/raw_ostream.h>

#import "UserRunner.h"

int main(int argc, const char *argv[]) {
    @autoreleasepool {
        if (argc == 1) {
            return [[[UserRunner alloc] initWithFile:nil] run];
        }

        for (int i = 1; i < argc; ++i) {
            llvm::errs() << "Running " << argv[i] << " ...\n";
            int res = [[[UserRunner alloc] initWithFile:@(argv[1])] run];
            llvm::errs() << "Returned with " << res << "\n\n";

            if (res != EXIT_SUCCESS) {
                llvm::errs() << "Terminating...\n";
                return res;
            }
        }
    }

    return 0;
}

//
//  EXCFunction.h
//  ti82
//
//  Created by Janek Spaderna on 11.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <llvm/ADT/ArrayRef.h>

#import "EXCTypes.h"

namespace EXC {

typedef std::pair<Type, bool> QualifiedType;

inline NSString *QualifiedTypeToString(QualifiedType ty) {
    NSString *tyStr = TypeToString(ty.first);
    return ty.second ? [@"&" stringByAppendingString:tyStr] : tyStr;
}

namespace param {
struct number {};
struct list {};
struct string {};

template <class T>
struct builder {};

template <>
struct builder<number> {
    constexpr static QualifiedType get() { return {TypeNumber, false}; }
};

template <>
struct builder<list> {
    constexpr static QualifiedType get() { return {TypeList, false}; }
};

template <>
struct builder<string> {
    constexpr static QualifiedType get() { return {TypeString, false}; }
};

template <class T>
struct builder<T &> {
    static_assert(!builder<T>::get().second, "cannot form reference to reference");
    constexpr static QualifiedType get() { return {builder<T>::get().first, true}; }
};
}

class FunctionSignature {
    std::vector<QualifiedType> argTys;

    template <Type Ty>
    static QualifiedType get_type() {
        return {Ty, false};
    }

public:
    FunctionSignature(std::vector<QualifiedType> &&types) : argTys(std::move(types)) {}
    FunctionSignature(llvm::ArrayRef<QualifiedType> types) : argTys(types) {}

    template <class... Types>
    static FunctionSignature get() {
        QualifiedType tys[sizeof...(Types)] = {param::builder<Types>::get()...};
        return FunctionSignature(tys);
    }

    const std::vector<QualifiedType> &argumentTypes() const { return argTys; }
    QualifiedType operator[](NSUInteger tyIdx) const { return argTys.at(tyIdx); }

    NSString *toString() const {
        bool first = true;
        NSMutableString *str = [@"(" mutableCopy];

        for (auto ty : argTys) {
            if (!first) {
                [str appendString:@", "];
            }

            first = false;
            [str appendString:QualifiedTypeToString(ty)];
        }

        return [str stringByAppendingString:@")"];
    }
};

typedef std::pair<FunctionSignature, id> FunctionOverload;
}

extern NSString *const CallRangeKey;

@interface EXCFunction : NSObject

+ (instancetype)functionWithOverloads:(llvm::ArrayRef<EXC::FunctionOverload>)overloads;
+ (instancetype)macroWithOverloads:(llvm::ArrayRef<EXC::FunctionOverload>)overloads;

- (instancetype)initWithOverloads:(llvm::ArrayRef<EXC::FunctionOverload>)overloads preevaluateArguments:(BOOL)preeval;

@property (nonatomic, readonly) llvm::ArrayRef<EXC::FunctionOverload> overloads;
@property (nonatomic) BOOL requiresEvaluatedArguments;

- (id<EXCRuntimeValue>)callWithArguments:(NSArray *)args inContext:(NSDictionary *)context;

@end

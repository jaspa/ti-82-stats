//
//  TI82Diagnosis.m
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82Diagnosis.h"

#include <llvm/ADT/SmallString.h>

#import "Functions.h"
#import "DiagnosisHandler.h"
#import "ColoredOstream.h"

#define DISPLAY_COLORS false

using namespace llvm;

namespace TI82 {

NS_INLINE raw_ostream::Colors matchingColor(Diagnosis::Kind k) {
    switch (k) {
        case Diagnosis::Notice: return raw_ostream::BLACK;
        case Diagnosis::Warning: return raw_ostream::YELLOW;
        case Diagnosis::Error: return raw_ostream::RED;
    }
}

NS_INLINE const char *stringFromKind(Diagnosis::Kind k) {
    switch (k) {
        case Diagnosis::Kind::Notice: return "note";
        case Diagnosis::Kind::Warning: return "warning";
        case Diagnosis::Kind::Error: return "error";
    }
}

void Diagnosis::deliver(id<DiagnosisHandler> receiver) && {
    [receiver handleDiagnosis:std::move(*this)];
}

void Diagnosis::render(raw_ostream &oss) const {
    ReferencingColoredOstream os(oss);

    struct Source {
        SourceRef fullLine;
        llvm::SmallString<90> annotation;
    };

    std::unique_ptr<Source> sourceAnnotation;

    if (errorRange.base) {
        os.changeColor(raw_ostream::BLACK, true) << errorRange.base->file().UTF8String << ':';

        if (!errorRange.range.empty()) {
            SourceRef src = errorRange.base->source();
            SourceRef before = src.substr(0, errorRange.range.ptr() - src.ptr());
            SourceRef after = src.substr(errorRange.range.ptr_end() - src.ptr());

            NSUInteger lineNumber = before.count('\n') + 1;
            NSUInteger lineStart = &before.find_last('\n').base() - src.ptr();

            auto nextBreak = after.find_first('\n');
            NSUInteger lineEnd = &(nextBreak == after.end() ? nextBreak : std::next(nextBreak)) - src.ptr();

            NSUInteger charNumber = src.substr(lineStart, errorRange.range.ptr() - src.ptr()).char32size() + 1;
            NSUInteger charNumEnd = charNumber + errorRange.range.char32size();

            SourceRef fullLine = src.substr(lineStart, lineEnd);
            llvm::SmallString<90> annotation;

            if (charNumber > 1) {
                annotation.append(charNumber - 1, ' ');
            }

            if (errorRange.hotPoint) {
                NSUInteger hotPointOffset = errorRange.hotPoint - errorRange.range.ptr();
                SourceRef before = errorRange.range.substr(0, hotPointOffset);
                SourceRef after = errorRange.range.substr(hotPointOffset + 1);

                annotation.append(before.char32size(), '~');
                annotation += '^';
                annotation.append(after.char32size(), '~');
            } else {
                annotation += '^';
                annotation.append(errorRange.range.char32size() - 1, '~');
            }

            sourceAnnotation = std::unique_ptr<Source>(new Source{fullLine, std::move(annotation)});

            os << lineNumber << ':' << charNumber;

            if (charNumEnd - charNumber > 1)
                os << ':' << charNumEnd;

            os << ": ";
        }
    }

    os.changeColor(matchingColor(kind), true) << stringFromKind(kind) << ": ";
    os.changeColor(raw_ostream::BLACK, kind != Notice) << message.UTF8String << '\n';
    os.resetColor();

    if (sourceAnnotation) {
        os << sourceAnnotation->fullLine.getNSString().UTF8String;
        os.changeColor(raw_ostream::GREEN, true) << sourceAnnotation->annotation.str() << '\n';
        os.resetColor();
    }

    for (NSString *note in notes) {
        os.changeColor(matchingColor(Notice), true) << stringFromKind(Notice) << ": ";
        os.resetColor() << note.UTF8String << '\n';
    }
}
}

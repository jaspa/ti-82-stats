//
//  TI82SourceRef.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 12.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <unicode/unistr.h>
#include <unicode/ustring.h>
#include <unicode/utf8.h>
#include <llvm/ADT/STLExtras.h>
#include <limits>
#include <algorithm>

namespace TI82 {

class SourceRef;

class SourceIterator : public std::iterator<std::bidirectional_iterator_tag, const char32_t> {
    std::unique_ptr<SourceRef> data;
    NSUInteger offset;

public:
    inline SourceIterator(SourceRef Src, bool end);

    SourceIterator(const SourceIterator &it) : data(std::make_unique<SourceRef>(*it.data)), offset(it.offset) {}

    SourceIterator &operator=(const SourceIterator &it) {
        data = std::make_unique<SourceRef>(*it.data);
        offset = it.offset;
        return *this;
    }

    inline char32_t operator*() const;

    inline bool operator==(const SourceIterator &it) const;
    bool operator!=(const SourceIterator &it) const { return !(*this == it); }

    inline SourceIterator &operator++();
    inline SourceIterator &operator--();

    SourceIterator operator++(int) {
        SourceIterator it(*this);
        ++*this;
        return it;
    }

    SourceIterator operator--(int) {
        SourceIterator it(*this);
        --*this;
        return it;
    }

    inline const char16_t *operator&() const;
};

class SourceRef {
public:
    typedef SourceIterator iterator;
    typedef SourceIterator const_iterator;
    typedef std::reverse_iterator<const_iterator> reverse_iterator;

private:
    NSUInteger Size = 0;
    const char16_t *Data = nullptr;

public:
#pragma mark - constructors
    constexpr SourceRef() = default;
    constexpr SourceRef(const char16_t *D, NSUInteger Sz) : Size(Sz), Data(D) {}
    constexpr SourceRef(const char16_t *I, const char16_t *E) : Size(E - I), Data(I) {}

    template <size_t N>
    constexpr SourceRef(const char16_t(&Str)[N])
        : Size(N - 1), Data(Str) {}

#pragma mark - regarding the size

    NSUInteger size() const { return Size; }
    NSUInteger char32size() const { return u_countChar32(ptr<UChar>(), static_cast<int32_t>(Size)); }

    bool empty() const { return Size == 0; }

#pragma mark - access the data

    const char16_t *ptr() const { return Data; }
    const char16_t *ptr_end() const { return Data + Size; }

    template <class T, class = typename std::enable_if<sizeof(T) == sizeof(char16_t)>::type>
    const T *ptr() const {
        return reinterpret_cast<const T *>(Data);
    }

    const_iterator begin() const { return {*this, false}; }
    const_iterator end() const { return {*this, true}; }

    reverse_iterator rbegin() const { return reverse_iterator(end()); }
    reverse_iterator rend() const { return reverse_iterator(begin()); }

    char16_t front() const {
        NSCParameterAssert(!empty());
        return Data[0];
    }

    char16_t back() const {
        NSCParameterAssert(!empty());
        return Data[Size - 1];
    }
    
    char32_t first() const {
        NSCParameterAssert(!empty());
        return *begin();
    }

    char16_t operator[](NSUInteger I) const {
        NSCAssert(I < Size, @"index %lu out of bounds 0..<%lu", I, (unsigned long)Size);
        return Data[I];
    }

    SourceRef substr(NSRange Range) const {
        NSCParameterAssert(NSMaxRange(Range) <= Size);
        return {Data + Range.location, Range.length};
    }

    SourceRef substr(NSUInteger From, NSUInteger To = NSUIntegerMax) const {
        return {Data + From, std::min(Size, To) - From};
    }

    SourceRef rebase(SourceRef onto) const { return {onto.Data, Size}; }

    NSString *getNSString(bool Copy = false) const {
        auto chars = ptr<unichar>();
        NSString *str = [NSString alloc];
        return Copy ? [str initWithCharacters:chars length:Size] :
                      [str initWithCharactersNoCopy:const_cast<unichar *>(chars) length:Size freeWhenDone:NO];
    }

    icu::UnicodeString getUniString(bool Copy = false) const {
        auto chars = ptr<UChar>();
        auto length = static_cast<int32_t>(size());
        return Copy ? icu::UnicodeString(chars, length) : icu::UnicodeString(false, chars, length);
    }

#pragma mark - algorithms

    NSUInteger count(char32_t c) const { return std::count(begin(), end(), c); }
    const_iterator find_first(char32_t c) const { return std::find(begin(), end(), c); }
    reverse_iterator find_last(char32_t c) const { return std::find(rbegin(), rend(), c); }

#pragma mark - equality

    bool is_equal(SourceRef S) const { return Data == S.Data && Size == S.Size; }
};

SourceIterator::SourceIterator(SourceRef Src, bool end)
    : data(std::make_unique<SourceRef>(Src)), offset(end ? Src.size() : 0) {
}

char32_t SourceIterator::operator*() const {
    NSCAssert(offset < data->size(), @"past-the-end iterator not dereferenceable");

    char32_t c;
    U16_GET(*data, 0, offset, data->size(), c);
    return c;
}

bool SourceIterator::operator==(const SourceIterator &it) const {
    return data->is_equal(*it.data) && offset == it.offset;
}

SourceIterator &SourceIterator::operator++() {
    U16_FWD_1(*data, offset, data->size());
    return *this;
}

SourceIterator &SourceIterator::operator--() {
    U16_BACK_1(*data, 0, offset);
    return *this;
}

const char16_t *SourceIterator::operator&() const {
    return data->ptr() + offset;
}

template <size_t N>
inline bool operator==(SourceRef Src, const char(&Str)[N]) {
    int i = 0;
    auto it = Src.begin(), end = Src.end();

    while (i < N && it != end) {
        char32_t u8c;
        U8_NEXT(Str, i, N, u8c);

        if (*it++ != u8c)
            return false;
    }

    return it == end && i == N;
}

template <size_t N>
inline bool operator!=(SourceRef Src, const char(&Str)[N]) {
    return !(Src == Str);
}

inline bool operator<(SourceRef x, SourceRef y) {
    return std::lexicographical_compare(x.begin(), x.end(), y.begin(), y.end());
}
}

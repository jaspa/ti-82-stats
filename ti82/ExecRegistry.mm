//
//  ExecRegistry.m
//  ti82
//
//  Created by Janek Spaderna on 17.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "ExecRegistry.h"

#import "ASTExecution.h"
#import "EXCFunction.h"
#import "TI82Diagnosis.h"

using namespace std;
using namespace EXC;

#define TI82_Error(msg, ...)                                                                                          \
    do {                                                                                                              \
        using TI82::Diagnosis;                                                                                        \
        Diagnosis::send(context[DiagnosisHandlerKey], Diagnosis::Error, [=](Diagnosis & diag) {                       \
            diag.message = [NSString stringWithFormat:(msg), ##__VA_ARGS__];                                          \
            diag.errorRange.base = reinterpret_cast<TI82::SourceHandler *>([context[SourceHandlerKey] pointerValue]); \
            if (NSValue *callRange = context[CallRangeKey]) {                                                         \
                NSRange range = callRange.rangeValue;                                                                 \
                diag.errorRange.range =                                                                               \
                    TI82::SourceRef(reinterpret_cast<const char16_t *>(uintptr_t(range.location)), range.length);     \
            }                                                                                                         \
        });                                                                                                           \
    } while (0)

typedef id (^UnaryBlock)(NSDictionary *, id);
typedef id (^BinaryBlock)(NSDictionary *, id, id);

namespace apply {

template <class UnaryFunction>
NS_INLINE UnaryBlock num(UnaryFunction fun) {
    return [^id(NSDictionary *, EXCNumber *n) {
      M_APM r = m_apm_init();
      fun(r, n.raw);
      return [EXCNumber makeNumber:r];
    } copy];
}

template <class UnaryFunction>
NS_INLINE UnaryBlock list(UnaryFunction fun) {
    return [^id(NSDictionary *, NSArray *ns) {
      NSMutableArray *rs = [NSMutableArray arrayWithCapacity:ns.count];

      for (EXCNumber *n in ns) {
          M_APM r = m_apm_init();
          fun(r, n.raw);
          [rs addObject:[EXCNumber makeNumber:r]];
      }

      return rs;
    } copy];
}

template <class BinaryFunction>
NS_INLINE BinaryBlock nums(BinaryFunction fun) {
    return [^id(NSDictionary *, EXCNumber *x, EXCNumber *y) {
      M_APM r = m_apm_init();
      fun(r, x.raw, y.raw);
      return [EXCNumber makeNumber:r];
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2, class BinaryFunction3>
NS_INLINE BinaryBlock nums(BinaryFunction1 fun, BinaryFunction2 checkX, BinaryFunction3 checkY) {
    return [^id(NSDictionary *context, EXCNumber *xx, EXCNumber *yy) {
      M_APM x = xx.raw, y = yy.raw;
      if (!checkX(context, x) || !checkY(context, y)) {
          M_APM r = m_apm_init();
          fun(r, x, y);
          [EXCNumber makeNumber:r];
      }
      return nil;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock nums1(BinaryFunction1 fun, BinaryFunction2 checkX) {
    return [^id(NSDictionary *context, EXCNumber *xx, EXCNumber *yy) {
      M_APM x = xx.raw, y = yy.raw;
      if (!checkX(context, x))
          return nil;

      M_APM r = m_apm_init();
      fun(r, x, y);
      return [EXCNumber makeNumber:r];
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock nums2(BinaryFunction1 fun, BinaryFunction2 checkY) {
    return [^id(NSDictionary *context, EXCNumber *xx, EXCNumber *yy) {
      M_APM x = xx.raw, y = yy.raw;
      if (!checkY(context, y))
          return nil;

      M_APM r = m_apm_init();
      fun(r, x, y);
      return [EXCNumber makeNumber:r];
    } copy];
}

template <class BinaryFunction>
NS_INLINE BinaryBlock num_list(BinaryFunction fun) {
    return [^id(NSDictionary *, EXCNumber *xx, NSArray *y) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM x = xx.raw;

      for (EXCNumber *y_n in y) {
          M_APM r = m_apm_init();
          fun(r, x, y_n.raw);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2, class BinaryFunction3>
NS_INLINE BinaryBlock num_list(BinaryFunction1 fun, BinaryFunction2 checkX, BinaryFunction3 checkY) {
    return [^id(NSDictionary *context, EXCNumber *xx, NSArray *y) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM x = xx.raw;
      if (!checkX(context, x))
          return nil;

      for (EXCNumber *yy_n in y) {
          M_APM y_n = yy_n.raw;
          if (!checkY(y_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x, y_n);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock num_list1(BinaryFunction1 fun, BinaryFunction2 checkX) {
    return [^id(NSDictionary *context, EXCNumber *xx, NSArray *y) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM x = xx.raw;
      if (!checkX(context, x))
          return nil;

      for (EXCNumber *y_n in y) {
          M_APM r = m_apm_init();
          fun(r, x, y_n.raw);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock num_list2(BinaryFunction1 fun, BinaryFunction2 checkY) {
    return [^id(NSDictionary *context, EXCNumber *xx, NSArray *y) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM x = xx.raw;

      for (EXCNumber *yy_n in y) {
          M_APM y_n = yy_n.raw;
          if (!checkY(context, y_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x, y_n);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction>
NS_INLINE BinaryBlock list_num(BinaryFunction fun) {
    return [^id(NSDictionary *, NSArray *x, EXCNumber *yy) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM y = yy.raw;

      for (EXCNumber *x_n in x) {
          M_APM r = m_apm_init();
          fun(r, x_n.raw, y);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2, class BinaryFunction3>
NS_INLINE BinaryBlock list_num(BinaryFunction1 fun, BinaryFunction2 checkX, BinaryFunction3 checkY) {
    return [^id(NSDictionary *context, NSArray *x, EXCNumber *yy) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM y = yy.raw;
      if (!checkY(context, y))
          return nil;

      for (EXCNumber *xx_n in x) {
          M_APM x_n = xx_n.raw;
          if (!checkX(context, x_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x_n, y);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock list_num1(BinaryFunction1 fun, BinaryFunction2 checkX) {
    return [^id(NSDictionary *context, NSArray *x, EXCNumber *yy) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM y = yy.raw;

      for (EXCNumber *xx_n in x) {
          M_APM x_n = xx_n.raw;
          if (!checkX(context, x_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x_n, y);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock list_num2(BinaryFunction1 fun, BinaryFunction2 checkY) {
    return [^id(NSDictionary *context, NSArray *x, EXCNumber *yy) {
      NSMutableArray *result = [NSMutableArray array];
      M_APM y = yy.raw;
      if (!checkY(context, y))
          return nil;

      for (EXCNumber *x_n in x) {
          M_APM r = m_apm_init();
          fun(r, x_n.raw, y);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction>
NS_INLINE BinaryBlock lists(BinaryFunction fun) {
    return [^id(NSDictionary *context, NSArray *x, NSArray *y) {
      NSUInteger count = x.count;
      if (count != y.count) {
          TI82_Error(@"DIMENSION MISMATCH (%lu vs %lu elements)", count, y.count);
          return nil;
      }

      NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];

      for (NSUInteger i = 0; i < count; ++i) {
          M_APM r = m_apm_init();
          fun(r, [x[i] raw], [y[i] raw]);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2, class BinaryFunction3>
NS_INLINE BinaryBlock lists(BinaryFunction1 fun, BinaryFunction2 checkX, BinaryFunction3 checkY) {
    return [^id(NSDictionary *context, NSArray *x, NSArray *y) {
      NSUInteger count = x.count;
      if (count != y.count) {
          TI82_Error(@"DIMENSION MISMATCH (%lu vs %lu elements)", count, y.count);
          return nil;
      }

      NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];

      for (NSUInteger i = 0; i < count; ++i) {
          M_APM x_n = [x[i] raw], y_n = [y[i] raw];
          if (!checkX(context, x_n) || !checkY(context, y_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x_n, y_n);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock lists1(BinaryFunction1 fun, BinaryFunction2 checkX) {
    return [^id(NSDictionary *context, NSArray *x, NSArray *y) {
      NSUInteger count = x.count;
      if (count != y.count) {
          TI82_Error(@"DIMENSION MISMATCH (%lu vs %lu elements)", count, y.count);
          return nil;
      }

      NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];

      for (NSUInteger i = 0; i < count; ++i) {
          M_APM x_n = [x[i] raw], y_n = [y[i] raw];
          if (!checkX(context, x_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x_n, y_n);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}

template <class BinaryFunction1, class BinaryFunction2>
NS_INLINE BinaryBlock lists2(BinaryFunction1 fun, BinaryFunction2 checkY) {
    return [^id(NSDictionary *context, NSArray *x, NSArray *y) {
      NSUInteger count = x.count;
      if (count != y.count) {
          TI82_Error(@"DIMENSION MISMATCH (%lu vs %lu elements)", count, y.count);
          return nil;
      }

      NSMutableArray *result = [NSMutableArray arrayWithCapacity:count];

      for (NSUInteger i = 0; i < count; ++i) {
          M_APM x_n = [x[i] raw], y_n = [y[i] raw];
          if (!checkY(context, y_n))
              return nil;

          M_APM r = m_apm_init();
          fun(r, x_n, y_n);
          [result addObject:[EXCNumber makeNumber:r]];
      }

      return result;
    } copy];
}
}

constexpr static const int DECIMAL_PLACES = 30;

@implementation ExecRegistry

- (EXCFunction *)Plus {
    BinaryBlock stringConcat = [^(NSDictionary *, EXCString *x, EXCString *y) {
      return [x stringByAppendingString:y];
    } copy];

    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(m_apm_add)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(m_apm_add)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(m_apm_add)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(m_apm_add)},
                               {FunctionSignature::get<param::string, param::string>(), stringConcat}}];
}

- (EXCFunction *)Minus {
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(m_apm_negate)},
                               {FunctionSignature::get<param::list>(), apply::list(m_apm_negate)},
                               {FunctionSignature::get<param::number, param::number>(), apply::nums(m_apm_subtract)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(m_apm_subtract)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(m_apm_subtract)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(m_apm_subtract)}}];
}

- (EXCFunction *)Multiply {
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(m_apm_multiply)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(m_apm_multiply)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(m_apm_multiply)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(m_apm_multiply)}}];
}

- (EXCFunction *)Divide {
    using namespace placeholders;
    auto divide = bind(m_apm_divide, _1, DECIMAL_PLACES, _2, _3);

    auto nonZero = [](NSDictionary *context, M_APM n) {
        if (m_apm_sign(n))
            return true;
        TI82_Error(@"DIVIDE BY ZERO");
        return false;
    };

    return [EXCFunction functionWithOverloads:
                            {{FunctionSignature::get<param::number, param::number>(), apply::nums2(divide, nonZero)},
                             {FunctionSignature::get<param::list, param::number>(), apply::list_num2(divide, nonZero)},
                             {FunctionSignature::get<param::number, param::list>(), apply::num_list2(divide, nonZero)},
                             {FunctionSignature::get<param::list, param::list>(), apply::lists2(divide, nonZero)}}];
}

- (EXCFunction *)Power {
    using namespace placeholders;
    auto pow = bind(m_apm_pow, _1, DECIMAL_PLACES, _2, _3);

    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(pow)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(pow)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(pow)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(pow)}}];
}

- (EXCFunction *)Equal {
    auto eq_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) == 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(eq_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(eq_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(eq_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(eq_func)}}];
}

- (EXCFunction *)Inequal {
    auto neq_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) != 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(neq_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(neq_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(neq_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(neq_func)}}];
}

- (EXCFunction *)Greater {
    auto comp_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) > 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)GreaterEqual {
    auto comp_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) >= 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)Less {
    auto comp_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) < 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)LessEqual {
    auto comp_func = [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_compare(x, y) <= 0 ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)And {
    auto comp_func =
        [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_sign(x) && m_apm_sign(y) ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)Or {
    auto comp_func =
        [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, m_apm_sign(x) || m_apm_sign(y) ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)Xor {
    auto comp_func =
        [](M_APM r, M_APM x, M_APM y) { m_apm_copy(r, !!m_apm_sign(x) ^ !!m_apm_sign(y) ? MM_One : MM_Zero); };
    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(comp_func)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(comp_func)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(comp_func)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(comp_func)}}];
}

- (EXCFunction *)Square {
    using namespace placeholders;
    auto square = bind(m_apm_integer_pow, _1, DECIMAL_PLACES, _2, 2);
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(square)},
                                               {FunctionSignature::get<param::list>(), apply::list(square)}}];
}

- (EXCFunction *)Cube {
    using namespace placeholders;
    auto cubic = bind(m_apm_integer_pow, _1, DECIMAL_PLACES, _2, 3);
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(cubic)},
                                               {FunctionSignature::get<param::list>(), apply::list(cubic)}}];
}

- (EXCFunction *)Reciprocal {
    using namespace placeholders;
    auto reciprocal = bind(m_apm_reciprocal, _1, DECIMAL_PLACES, _2);
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(reciprocal)},
                                               {FunctionSignature::get<param::list>(), apply::list(reciprocal)}}];
}

- (EXCFunction *)Factorial {
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(m_apm_factorial)},
                                               {FunctionSignature::get<param::list>(), apply::list(m_apm_factorial)}}];
}

- (EXCFunction *)SquareRoot {
    using namespace placeholders;
    auto root = bind(m_apm_sqrt, _1, DECIMAL_PLACES, _2);
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(root)},
                                               {FunctionSignature::get<param::list>(), apply::list(root)}}];
}

- (EXCFunction *)CubicRoot {
    using namespace placeholders;
    auto root = bind(m_apm_cbrt, _1, DECIMAL_PLACES, _2);
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(root)},
                                               {FunctionSignature::get<param::list>(), apply::list(root)}}];
}

- (EXCFunction *)NthRoot {
    auto root = [](M_APM r, M_APM n, M_APM x) {
        // n x√ m == m ^ (1/n)
        auto h = m_apm_init();
        m_apm_divide(h, DECIMAL_PLACES, MM_One, n);
        m_apm_pow(r, DECIMAL_PLACES, x, h);
        m_apm_free(h);
    };

    return [EXCFunction
        functionWithOverloads:{{FunctionSignature::get<param::number, param::number>(), apply::nums(root)},
                               {FunctionSignature::get<param::list, param::number>(), apply::list_num(root)},
                               {FunctionSignature::get<param::number, param::list>(), apply::num_list(root)},
                               {FunctionSignature::get<param::list, param::list>(), apply::lists(root)}}];
}

- (EXCFunction *)Dimension {
    auto dim = ^(NSDictionary *, NSArray *list) {
      auto r = m_apm_init();
      m_apm_set_long(r, list.count);
      return [EXCNumber makeNumber:r];
    };

    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::list>(), dim}}];
}

- (EXCFunction *)Not {
    auto not_func = [](M_APM result, M_APM x) { m_apm_copy(result, m_apm_sign(x) == 0 ? MM_One : MM_Zero); };
    return [EXCFunction functionWithOverloads:{{FunctionSignature::get<param::number>(), apply::num(not_func)},
                                               {FunctionSignature::get<param::list>(), apply::list(not_func)}}];
}

@end

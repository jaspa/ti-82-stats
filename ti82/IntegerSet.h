//
//  IntegerSet.h
//  ti82
//
//  Created by Janek Spaderna on 23.01.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

#import "Types.h"
#import <llvm/ADT/SmallVector.h>
#import <llvm/ADT/iterator_range.h>
#import <Foundation/Foundation.h>
#import <vector>

template <class T>
struct Range {
    typedef T value_type;
    value_type location = 0;
    value_type length = 0;

    constexpr bool empty() const { return length == 0; }

    constexpr value_type last() const { return location + length - 1; }
    constexpr value_type upper() const { return location + length; }

    constexpr bool includes(value_type elem) const { return elem >= location && elem < upper(); }
    constexpr bool includes(Range range) const { return includes(range.location) && includes(range.last()); }

    constexpr bool overlaps(Range range) const {
        return includes(range.location) || includes(range.last()) || range.includes(location);
    }

    constexpr bool touches(Range range) const { return upper() == range.location || range.upper() == location; }

    void extend(value_type idx) {
        value_type upp = std::max(last(), idx) + 1;
        location = std::min(location, idx);
        length = upp - location;
    }

    void extend(Range range) {
        extend(range.location);
        extend(range.last());
    }
};

template <class T>
class IntegerSet {
public:
    typedef T value_type;
    typedef Range<value_type> range_type;

private:
    typedef llvm::SmallVector<range_type, 4> storage_type;

private:
    storage_type ranges_;

private:
#if DEBUG
    void check_internal() {
        bool first = true;
        value_type last = 0;

        for (range_type r : ranges_) {
            NSCAssert(r.length > 0, @"Bad range length");

            if (!first)
                NSCAssert(r.location > last, @"Overlapping or touching ranges");

            first = false;
            last = r.upper();
        }
    }

#define INJECT_CHECK() at_scope_end = std::bind(&IntegerSet::check_internal, this)
#else
#define INJECT_CHECK()
#endif

    typename storage_type::iterator find(value_type i) {
        return std::lower_bound(ranges_.begin(), ranges_.end(), i,
                                [](range_type r, value_type i) { return r.upper() < i; });
    }

public:
    IntegerSet() { INJECT_CHECK(); }

    bool contains(value_type i) {
        auto it = find(i);
        return it != ranges_.end() && it->includes(i);
    }

    void add(value_type i) { add({i, 1}); }

    void add(range_type range) {
        if (range.length < 0)
            throw std::domain_error("range length less than 0");

        INJECT_CHECK();

        if (range.length == 0)
            return;

        if (ranges_.empty()) {
            ranges_.push_back(range);
            return;
        }

        auto bound = find(range.location);

        if (bound == ranges_.end()) {
            // No merging needed
            ranges_.push_back(range);
            return;
        }

        if (bound->overlaps(range)) {
            bound->extend(range);
        } else {
            bound = ranges_.insert(bound, range);
        }

        auto after = std::next(bound);
        auto last =
            std::find_if_not(after, ranges_.end(), std::bind(&range_type::overlaps, *bound, std::placeholders::_1));

        if (after != last) {
            bound->extend(*std::prev(last));
            ranges_.erase(after, last);
        }
    }

    void clear() {
        INJECT_CHECK();
        ranges_.clear();
    }

#undef INJECT_CHECK
};

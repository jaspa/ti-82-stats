//
//  ElementIterator.h
//  ti82
//
//  Created by Janek Spaderna on 16.12.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#include <iterator>

template <class Iter,
          class Func,
          class Element =
              std::remove_reference_t<std::result_of_t<Func(typename std::iterator_traits<Iter>::value_type)>>>
class ElementIterator : public std::iterator<typename std::iterator_traits<Iter>::iterator_category,
                                             Element,
                                             typename std::iterator_traits<Iter>::difference_type> {
public:
    typedef Iter iterator_type;
    typedef typename std::iterator_traits<Iter>::difference_type difference_type;
    typedef Element &reference;
    typedef Element *pointer;

private:
    Iter base_;
    Func elemFunc;

public:
    ElementIterator(Iter base, Func fun) : base_(base), elemFunc(fun) {}

    Iter base() const { return base_; }

    bool operator<(const ElementIterator &it) const { return base_ < it.base_; }
    bool operator>(const ElementIterator &it) const { return base_ > it.base_; }
    bool operator<=(const ElementIterator &it) const { return base_ <= it.base_; }
    bool operator>=(const ElementIterator &it) const { return base_ >= it.base_; }
    bool operator==(const ElementIterator &y) const { return base_ == y.base_; }
    bool operator!=(const ElementIterator &y) const { return base_ != y.base_; }

    reference operator[](difference_type n) { return base_[n].*elemFunc; }
    reference operator[](difference_type n) const { return base_[n].*elemFunc; }
    reference operator*() { return (*base_).*elemFunc; }
    reference operator*() const { return (*base_).*elemFunc; }
    pointer operator->() { return &(*base_).*elemFunc; }
    const pointer operator->() const { return &(*base_).*elemFunc; }

    ElementIterator &operator++() {
        ++base_;
        return *this;
    }

    ElementIterator &operator++(int) {
        ElementIterator it(*this);
        ++*this;
        return it;
    }

    ElementIterator &operator--() {
        --base_;
        return *this;
    }

    ElementIterator &operator--(int) {
        ElementIterator it(*this);
        --*this;
        return it;
    }

    ElementIterator operator+(difference_type n) const { return {base_ + n, elemFunc}; }
    ElementIterator &operator+=(difference_type n) {
        base_ += n;
        return *this;
    }

    ElementIterator operator-(difference_type n) const { return {base_ - n, elemFunc}; }
    ElementIterator &operator-=(difference_type n) {
        base_ -= n;
        return *this;
    }

    difference_type operator-(const ElementIterator &it) const { return base_ - it.base_; }
};

template <class I, class F, class E>
ElementIterator<I, F, E> operator+(typename ElementIterator<I, F, E>::difference n,
                                   const ElementIterator<I, F, E> &it) {
    return it + n;
}

template <class I, class F>
ElementIterator<I, F> make_element_iterator(I it, F f) {
    return {it, f};
}

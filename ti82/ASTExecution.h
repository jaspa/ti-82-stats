//
//  ASTExecution.h
//  ti82
//
//  Created by Janek Spaderna on 05.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82AST.h"
#import "EXCTypes.h"

extern NSString *const DiagnosisHandlerKey;
extern NSString *const VariablesDictionaryKey;
extern NSString *const SourceHandlerKey;
extern NSString *const FunctionRegistryKey;

@interface ASTNode (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context;

@end

@interface ASTVarRef (Execution)

- (id<EXCRuntimeValue>)executionDefaultValueInContext:(NSDictionary *)context;

@end

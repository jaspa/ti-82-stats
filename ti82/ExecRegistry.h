//
//  ExecRegistry.h
//  ti82
//
//  Created by Janek Spaderna on 17.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "FunctionRegistry.h"

@interface ExecRegistry : FunctionRegistry

@end

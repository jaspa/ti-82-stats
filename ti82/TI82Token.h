//
//  TI82Token.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 13.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <llvm/ADT/ArrayRef.h>
#include <llvm/ADT/Optional.h>
#include <vector>

#import "TI82SourceRef.h"
#import "TokenTable.h"

struct MatchedToken : public Tok {
    MatchedToken() : Tok() {}
    MatchedToken(Tok base, llvm::ArrayRef<TI82::SourceRef> captures) : Tok(base), Groups(captures) {}

    TI82::SourceRef source() const { return Groups.front(); }

    llvm::Optional<TI82::SourceRef> operator[](size_t group) const {
        return group < Groups.size() ? Groups[group] : llvm::Optional<TI82::SourceRef>();
    }

    const std::vector<TI82::SourceRef> &groups() const { return Groups; }
    size_t groupCount() const { return Groups.size(); }

private:
    std::vector<TI82::SourceRef> Groups;
};

//
//  SourceMatcher.h
//  ti82
//
//  Created by Janek Spaderna on 19.01.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <llvm/ADT/SmallVector.h>
#include <unicode/regex.h>
#include <algorithm>

#import "TI82SourceRef.h"
#import "ICUAdditions.h"

namespace TI82 {

class SourceMatcher {
    virtual void _anchor();

public:
    virtual ~SourceMatcher() = default;

    virtual bool match(NSUInteger offset, SourceRef src, llvm::SmallVectorImpl<SourceRef> &groups) const = 0;
};

class CompareMatcher : public SourceMatcher {
    virtual void _anchor();
    SourceRef Pattern;

public:
    CompareMatcher(SourceRef pat) : Pattern(pat) {}

    bool match(NSUInteger offset, SourceRef src, llvm::SmallVectorImpl<SourceRef> &groups) const override {
        src = src.substr(offset);
        auto patEnd = Pattern.end();
        auto mis = std::mismatch(Pattern.begin(), patEnd, src.begin(), src.end());

        if (mis.first != patEnd)
            return false;

        groups.push_back(Pattern.rebase(src));
        return true;
    }
};

class RegexMatcher : public SourceMatcher {
    virtual void _anchor();
    std::unique_ptr<icu::RegexMatcher> Regex;
    icu::UnicodeString src;

public:
    RegexMatcher(SourceRef pat, SourceRef src_) : src(src_.getUniString()) {
        UErrorCode err = U_ZERO_ERROR;
        Regex = std::make_unique<icu::RegexMatcher>(pat.getUniString(), src, 0, err);
        ASSERT_SUCCESS(err, @"cannot create matcher for pattern %@", pat.getNSString());
    }

    // If `src` is different from the `src` passed in the constructor, the behaviour is undefined
    bool match(NSUInteger offset, SourceRef src, llvm::SmallVectorImpl<SourceRef> &groups) const override {
        UErrorCode err = U_ZERO_ERROR;

        if (!Regex->lookingAt(offset, err)) {
            // Assume there can only be an error if `lookingAt` returned false
            ASSERT_SUCCESS(err, @"cannot match regex");
            return false;
        }

        for (auto i = 0, e = Regex->groupCount() + 1; i < e; ++i) {
            auto start = Regex->start64(i, err);
            ASSERT_SUCCESS(err, @"cannot retrieve start index of match group %d", i);
            auto end = Regex->end64(i, err);
            ASSERT_SUCCESS(err, @"cannot retrieve end index of match group %d", i);
            groups.push_back(src.substr(start, end));
        }

        return true;
    }
};
}

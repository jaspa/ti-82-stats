//
//  SourceMatcher.m
//  ti82
//
//  Created by Janek Spaderna on 19.01.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

#import "SourceMatcher.h"

namespace TI82 {

// Provide anchors for the vtable
void SourceMatcher::_anchor() {
}

void CompareMatcher::_anchor() {
}

void RegexMatcher::_anchor() {
}
}

//
//  ColoredOstream.h
//  ti82
//
//  Created by Janek Spaderna on 14.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include <memory>
#include <llvm/Support/raw_ostream.h>

class ReferencingColoredOstream : public llvm::raw_ostream {
    static bool xcodeColoring;

    llvm::raw_ostream &os;
    const char *bgColor;
    const char *fgColor;

    ReferencingColoredOstream(llvm::raw_ostream &os, const char *bg, const char *fg)
        : llvm::raw_ostream(true), os(os), bgColor(bg), fgColor(fg) {}

public:
    static ReferencingColoredOstream ref(llvm::raw_ostream &os) { return std::move(ReferencingColoredOstream(os)); }

    explicit ReferencingColoredOstream(llvm::raw_ostream &os) : ReferencingColoredOstream(os, nullptr, nullptr) {}

    ReferencingColoredOstream(ReferencingColoredOstream &&os)
        : ReferencingColoredOstream(os.os, os.bgColor, os.fgColor) {}

#define FWD_FUNCTION(R, name, spec, ...) \
    R name spec { return os.name(__VA_ARGS__); }

#define CHAINING_FUNCTION(name, spec, ...) \
    ReferencingColoredOstream &name spec { \
        os.name(__VA_ARGS__);              \
        return *this;                      \
    }

#define VOID_FUNCTION(name, spec, ...) \
    void name spec { os.name(__VA_ARGS__); }

    FWD_FUNCTION(uint64_t, tell, () const)

    VOID_FUNCTION(SetBuffered, ())
    VOID_FUNCTION(SetBufferSize, (size_t Size), Size)
    FWD_FUNCTION(size_t, GetBufferSize, () const)
    VOID_FUNCTION(SetUnbuffered, ())
    FWD_FUNCTION(size_t, GetNumBytesInBuffer, () const)

    VOID_FUNCTION(flush, ())
    CHAINING_FUNCTION(operator<<, (char C), C)
    CHAINING_FUNCTION(operator<<, (unsigned char C), C)
    CHAINING_FUNCTION(operator<<, (signed char C), C)
    CHAINING_FUNCTION(operator<<, (llvm::StringRef S), S)
    CHAINING_FUNCTION(operator<<, (const char *S), S)
    CHAINING_FUNCTION(operator<<, (const std::string &S), S)
    CHAINING_FUNCTION(operator<<, (unsigned long N), N)
    CHAINING_FUNCTION(operator<<, (long N), N)
    CHAINING_FUNCTION(operator<<, (unsigned long long N), N)
    CHAINING_FUNCTION(operator<<, (long long N), N)
    CHAINING_FUNCTION(operator<<, (const void *P), P)
    CHAINING_FUNCTION(operator<<, (unsigned int N), N)
    CHAINING_FUNCTION(operator<<, (int N), N)
    CHAINING_FUNCTION(operator<<, (double N), N)
    CHAINING_FUNCTION(operator<<, (const llvm::format_object_base &Fmt), Fmt)
    CHAINING_FUNCTION(operator<<, (const llvm::FormattedString &S), S)
    CHAINING_FUNCTION(operator<<, (const llvm::FormattedNumber &N), N)


    CHAINING_FUNCTION(write_hex, (unsigned long long N), N)
    CHAINING_FUNCTION(write_escaped, (llvm::StringRef S, bool UseH = false), S, UseH)
    CHAINING_FUNCTION(write, (unsigned char C), C)
    CHAINING_FUNCTION(write, (const char *Ptr, size_t Size), Ptr, Size)

    CHAINING_FUNCTION(indent, (unsigned Num), Num)

    FWD_FUNCTION(bool, is_displayed, () const);

#undef FWD_FUNCTION
#undef VOID_FUNCTION
#undef CHAINING_FUNCTION

    static bool XcodeColoring() { return xcodeColoring; }
    virtual bool has_colors() const override { return xcodeColoring || os.has_colors(); }

    virtual ReferencingColoredOstream &resetColor() override {
        if (xcodeColoring) {
            fgColor = bgColor = nil;
            return *this << "\033[;";
        }

        os.resetColor();
        return *this;
    }

    virtual ReferencingColoredOstream &reverseColor() override {
        if (xcodeColoring) {
            std::swap(fgColor, bgColor);
            return *this << "\033[fg" << (fgColor ?: "") << ";\033[bg" << (bgColor ?: "") << ';';
        }

        os.reverseColor();
        return *this;
    }

    virtual ReferencingColoredOstream &changeColor(enum Colors Color, bool Bold = false, bool BG = false) override {
        if (!xcodeColoring) {
            os.changeColor(Color, Bold, BG);
            return *this;
        }

        const char *&store = (BG ? bgColor : fgColor);
        switch (Color) {
            case BLACK: store = "0,0,0"; break;
            case RED: store = "255,0,0"; break;
            case GREEN: store = "0,255,0"; break;
            case YELLOW: store = "255,255,0"; break;
            case BLUE: store = "0,0,255"; break;
            case MAGENTA: store = "255,0,255"; break;
            case CYAN: store = "0,255,255"; break;
            case WHITE: store = "255,255,255"; break;
            case SAVEDCOLOR: return *this;
        }

        return *this << "\033[" << (BG ? "bg" : "fg") << store << ';';
    }


protected:
    virtual void write_impl(const char *Ptr, size_t Size) override {
        fprintf(stderr, "%s should never be called", __func__);
        abort();
    }

    virtual uint64_t current_pos() const override {
        fprintf(stderr, "%s should never be called", __func__);
        abort();
    }
};


template <class Stream>
class ColoredOstream : public Stream {
    static_assert(std::is_base_of<llvm::raw_ostream, Stream>::value, "Stream must be a subclass of llvm::raw_ostream");

    dispatch_once_t colorHandlerPred = 0;
    std::unique_ptr<ReferencingColoredOstream> colorHandler;

    ReferencingColoredOstream &getColorHandler() {
        dispatch_once(&colorHandlerPred, ^{
          colorHandler = std::make_unique<ReferencingColoredOstream>(*this);
        });
        return *colorHandler;
    }

public:
    using Stream::Stream;

    virtual ColoredOstream &changeColor(enum Stream::Colors Color, bool Bold = false, bool BG = false) override {
        getColorHandler().changeColor(Color, Bold, BG);
        return *this;
    }

    virtual ColoredOstream &reverseColor() override {
        getColorHandler().reverseColor();
        return *this;
    }

    virtual ColoredOstream &resetColor() override {
        getColorHandler().resetColor();
        return *this;
    }

    virtual bool has_colors() const override {
        return ReferencingColoredOstream::XcodeColoring() || Stream::has_colors();
    }
};

//
//  EXCTypesAST.mm
//  ti82
//
//  Created by Janek Spaderna on 08.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "EXCTypes.h"

@implementation ASTFunctionCall (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeDynamic;
}

@end

@implementation ASTNumberLiteral (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeNumber;
}

@end

@implementation ASTListLiteral (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeList;
}

@end

@implementation ASTStringLiteral (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeString;
}

@end

@implementation ASTNumberVarRef (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeNumber;
}

@end

@implementation ASTListVarRef (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeList;
}

@end

@implementation ASTStringVarRef (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeString;
}

@end

@implementation ASTAnsVarRef (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeDynamic;
}

@end

@implementation ASTDisplayStmt (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeNull;
}

@end

@implementation ASTProgramCall (EXCTypedObject)

- (EXC::Type)exc_type {
    return EXC::TypeNull;
}

@end

@implementation ASTStoreStmt (EXCTypedObject)

- (EXC::Type)exc_type {
    return self.location.exc_type;
}

@end

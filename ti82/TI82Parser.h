//
//  TI82Parser.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 16.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82AST.h"
#import "DiagnosisHandler.h"

namespace TI82 {

class Parser {
    void *impl;

public:
    explicit Parser(SourceHandler &FullSource, id<DiagnosisHandler> DiagHandler = nil);
    ~Parser();

    ASTNode *ParseNode();
    void skipToken();

    bool completed() const;
    explicit operator bool() const { return !completed(); }
};
}

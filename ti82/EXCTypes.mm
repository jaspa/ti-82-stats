//
//  EXCTypes.m
//  ti82
//
//  Created by Janek Spaderna on 05.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "EXCTypes.h"

#pragma mark - Types

namespace EXC {

NSString *TypeToString(Type t) {
    switch (t) {
        case TypeNumber: return @"number";
        case TypeList: return @"list";
        case TypeMatrix: return @"matrix";
        case TypeString: return @"string";
        case TypeNull: return @"null";
        case TypeDynamic: return @"dynamic";
    }
}
}

#pragma mark - Numbers

@implementation EXCNumber {
    BOOL _free;
}

- (instancetype)init {
    return [self initWithRawValue:MM_Zero freeWhenDone:NO];
}

+ (instancetype)makeNumber:(M_APM)raw {
    return [[self alloc] initWithRawValue:raw freeWhenDone:YES];
}

+ (instancetype)borrowNumber:(M_APM)raw {
    return [[self alloc] initWithRawValue:raw freeWhenDone:NO];
}

- (instancetype)initWithRawValue:(M_APM)raw freeWhenDone:(BOOL)free {
    if ((self = [super init])) {
        _free = free;
        _raw = raw;
    }
    return self;
}

- (void)dealloc {
    if (_free)
        m_apm_free(_raw);
}

- (EXC::Type)exc_type {
    return EXC::TypeNumber;
}

- (NSString *)exc_stringRepresentation {
    char *str = static_cast<char *>(calloc(1, 32));

    M_APM conv = m_apm_init();
    m_apm_round(conv, 10, _raw);
    m_apm_to_string(str, -1, conv);
    m_apm_free(conv);

    return [[NSString alloc] initWithBytesNoCopy:str length:strlen(str) encoding:NSUTF8StringEncoding freeWhenDone:YES];
}

- (NSString *)description {
    return self.exc_stringRepresentation;
}

@end

#pragma mark - Lists

@implementation NSArray (EXCValue)

- (EXC::Type)exc_type {
    return EXC::TypeList;
}

- (NSString *)exc_stringRepresentation {
    BOOL first = YES;
    NSMutableString *string = [NSMutableString stringWithString:@"{ "];

    for (EXCNumber *num in self) {
        if (!first)
            [string appendString:@", "];

        first = NO;
        [string appendString:num.exc_stringRepresentation];
    }

    return [string stringByAppendingString:@" }"];
}

@end

#pragma mark - Strings

@implementation EXCString {
    std::vector<uint32_t> _indices;
}

+ (instancetype)makeStringWithData:(NSString *)data indices:(llvm::ArrayRef<uint32_t>)indices {
    return [[self alloc] initWithData:data indices:indices];
}

- (instancetype)initWithData:(NSString *)data indices:(llvm::ArrayRef<uint32_t>)indices {
    if ((self = [super init])) {
        _data = [data copy];
        _indices = indices;
    }
    return self;
}

- (llvm::ArrayRef<uint32_t>)indices {
    return _indices;
}

- (EXC::Type)exc_type {
    return EXC::TypeString;
}

- (NSString *)exc_stringRepresentation {
    return _data;
}

- (NSString *)description {
    return self.exc_stringRepresentation;
}

- (EXCString *)stringByAppendingString:(EXCString *)str {
    EXCString *concat = [[EXCString alloc] initWithData:[self.data stringByAppendingString:str.data] indices:_indices];
    concat->_indices.insert(concat->_indices.end(), str->_indices.begin(), str->_indices.end());
    return concat;
}

@end

#pragma mark - Null

@implementation NSNull (EXCValue)

- (EXC::Type)exc_type {
    return EXC::TypeNull;
}

- (NSString *)exc_stringRepresentation {
    return @"<null>";
}

@end

//
//  TI82Tokenizer.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 13.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TI82SourceRef.h"
#import "TI82SourceHandler.h"
#import "TI82Token.h"
#import "SourceMatcher.h"

class Tokenizer {
    typedef std::unique_ptr<TI82::SourceMatcher> MatcherType;
    typedef MatcherType &MatcherRef;

    NSUInteger Cursor;
    MatchedToken CurTok;
    std::vector<MatcherType> Matchers;
    TI82::SourceHandler &SrcHandler;

public:
    explicit Tokenizer(TI82::SourceHandler &src) : Cursor(0), SrcHandler(src) {}

    const MatchedToken &getCurrentToken() const { return CurTok; }

    template <class UnaryFunction>
    const MatchedToken &getNextToken(UnaryFunction Validator) {
        if (finished())
            return CurTok = MatchedToken();

        Tok tok;
        llvm::SmallVector<TI82::SourceRef, 4> groups;
        auto source = SrcHandler.source();

        for (auto it = Matchers.begin(), end = Matchers.end(); it != end; ++it) {
            tok.moveNext();
            if (Validator(tok) && (*it)->match(Cursor, source, groups))
                break;
        }

        while (groups.empty() && tok.moveNext()) {
            Matchers.push_back(tok.createMatcher(source));
            Matchers.back()->match(Cursor, source, groups);
        }

        if (groups.empty())
            return CurTok = TokenFallback ? TokenFallback(Cursor, source) : MatchedToken();

        Cursor += groups.front().size();
        return CurTok = MatchedToken(tok, groups);
    }

    const MatchedToken &getNextToken() {
        return getNextToken([](Tok) { return true; });
    }

    TI82::SourceHandler &sourceHandler() { return SrcHandler; }

    bool finished() const { return Cursor >= SrcHandler.source().size(); }

    std::function<MatchedToken(NSUInteger &, TI82::SourceRef)> TokenFallback;
};

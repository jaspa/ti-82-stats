//
//  TI82AST.m
//  TI-82 STATS
//
//  Created by Janek Spaderna on 16.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82AST.h"

using namespace TI82;

@interface ASTNode ()
@property (nonatomic, readonly, copy) NSString *dataString;
@end

#pragma mark -

@implementation ASTNode

+ (instancetype)nodeWithSource:(TI82::SourceRef)source {
    return [(ASTNode *)[self alloc] initWithSource:source];
}

- (instancetype)init {
    return self = [self initWithSource:{}];
}

- (instancetype)initWithSource:(SourceRef)source {
    if ((self = [super init])) {
        _source = source;
    }
    return self;
}

- (NSString *)debugDescription {
    NSString *base =
        [NSString stringWithFormat:@"<AST::%@ ‘%@’", [self.className substringFromIndex:3], self.source.getNSString()];

    if (NSString *data = self.dataString)
        return [base stringByAppendingFormat:@" => %@ >", data];
    else
        return [base stringByAppendingString:@">"];
}

- (NSString *)dataString {
    return nil;
}

- (BOOL)mayFormReference {
    return NO;
}

@end

#pragma mark - Expressions

@implementation ASTExpression
@end

@implementation ASTFunctionCall

+ (instancetype)functionCallWithID:(Tok)funcID arguments:(NSArray *)args source:(TI82::SourceRef)source {
    ASTFunctionCall *f = [self nodeWithSource:source];

    if (f) {
        f->_arguments = [args copy];
        f->_functionID = funcID;
    }

    return f;
}

@end

#pragma mark Literals

@implementation ASTNumberLiteral

+ (instancetype)numberLiteralWithValue:(M_APM)val source:(TI82::SourceRef)source {
    ASTNumberLiteral *n = [self nodeWithSource:source];

    if (n) {
        n->_numValue = val;
    }

    return n;
}

- (NSString *)dataString {
    NSUInteger length = self.numValue->m_apm_datalength + 10;
    char *num = static_cast<char *>(calloc(1, length));
    m_apm_to_fixpt_string(num, -1, self.numValue);
    return [[NSString alloc] initWithBytesNoCopy:num length:length encoding:NSUTF8StringEncoding freeWhenDone:YES];
}

- (void)dealloc {
    m_apm_free(_numValue);
}

@end

@implementation ASTListLiteral

+ (instancetype)listLiteralWithElements:(NSArray *)elems source:(SourceRef)source {
    ASTListLiteral *l = [self nodeWithSource:source];

    if (l) {
        l->_elements = [elems copy];
    }

    return l;
}

@end

@implementation ASTStringLiteral {
    std::vector<uint32_t> _userIndices;
}

+ (instancetype)stringLiteralWithUserIndices:(llvm::ArrayRef<uint32_t>)indices source:(TI82::SourceRef)source {
    ASTStringLiteral *s = [self nodeWithSource:source];

    if (s) {
        s->_userIndices = indices;
    }

    return s;
}

- (llvm::ArrayRef<uint32>)userIndices {
    return _userIndices;
}

- (SourceRef)content {
    auto source = self.source;
    return source.substr(NSMakeRange(1, source.back() == '"' ? source.size() - 2 : source.size() - 1));
}

@end

#pragma mark Variables

@implementation ASTVarRef

+ (instancetype)varRefWithName:(TI82::SourceRef)name {
    return [self nodeWithSource:name];
}

- (NSString *)name {
    return self.source.getNSString(true);
}

- (BOOL)isEqual:(id)object {
    return [object isMemberOfClass:[self class]] && [[object name] isEqualToString:self.name];
}

- (NSUInteger)hash {
    return [self.name hash];
}

- (BOOL)mayFormReference {
    return YES;
}

@end

@implementation ASTNumberVarRef
@end

@implementation ASTListVarRef
@end

@implementation ASTStringVarRef
@end

@implementation ASTAnsVarRef

- (NSString *)name {
    return ASTAnsVarName;
}

- (BOOL)mayFormReference {
    return NO;
}

@end

NSString *const ASTAnsVarName = @"Ans";

#pragma mark - Statements

@implementation ASTStatement
@end

@implementation ASTBlockStatement

+ (instancetype)blockWithNodes:(NSArray *)nodes source:(TI82::SourceRef)source {
    ASTBlockStatement *s = [self nodeWithSource:source];

    if (s) {
        s->_nodes = [nodes copy];
    }

    return s;
}

@end

@implementation ASTIfStatement

+ (instancetype) if:(ASTExpression *)condition
               then:(ASTBlockStatement *)thenBlock
               else:(ASTBlockStatement *)elseBlock
             source:(TI82::SourceRef)source {

    ASTIfStatement *s = [self nodeWithSource:source];

    if (s) {
        s->_condition = condition;
        s->_thenBlock = thenBlock;
        s->_elseBlock = elseBlock;
    }

    return s;
}

@end

@implementation ASTLoopStatement

- (instancetype)initWithBody:(ASTBlockStatement *)body source:(TI82::SourceRef)source {
    if (self = [super initWithSource:source]) {
        _body = body;
    }

    return self;
}

@end

@implementation ASTConditionedLoopStatement

+ (instancetype)loopOverBlock:(ASTBlockStatement *)body condition:(ASTExpression *)condition source:(SourceRef)source {
    ASTConditionedLoopStatement *s = [[self alloc] initWithBody:body source:source];

    if (s) {
        s->_condition = condition;
    }

    return s;
}

@end

@implementation ASTWhileLoop
@end

@implementation ASTRepeatLoop
@end

@implementation ASTForLoop

+ (instancetype)loopFor:(ASTNumberVarRef *)var
                   from:(ASTExpression *)start
                     to:(ASTExpression *)end
                     by:(ASTExpression *)stepper
                     do:(ASTBlockStatement *)body
                 source:(TI82::SourceRef)source {

    ASTForLoop *l = [[self alloc] initWithBody:body source:source];

    if (l) {
        l->_variable = var;
        l->_start = start;
        l->_end = end;
        l->_stepper = stepper;
    }

    return l;
}

@end

@implementation ASTDisplayStmt

+ (instancetype)displayExpressions:(NSArray *)exprs source:(SourceRef)source {
    ASTDisplayStmt *s = [self nodeWithSource:source];

    if (s) {
        s->_expressions = [exprs copy];
    }

    return s;
}

@end

@implementation ASTProgramCall

+ (instancetype)programmCallWithSource:(TI82::SourceRef)source {
    return [self nodeWithSource:source];
}

- (NSString *)name {
    // strip the 'prgm'...
    return self.source.substr(4).getNSString(true);
}

@end

@implementation ASTStoreStmt

+ (instancetype)storeExpression:(ASTExpression *)expr intoVariable:(ASTVarRef *)var source:(SourceRef)source {
    ASTStoreStmt *s = [self nodeWithSource:source];

    if (s) {
        s->_expression = expr;
        s->_location = var;
    }

    return s;
}

@end

//
//  TI82SourceHandler.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 26.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82SourceRef.h"

namespace TI82 {

class SourceHandler {
private:
    SourceRef Base;
    NSString *File;

    SourceHandler(const SourceHandler &) = delete;

public:
    constexpr static NSString *const MainModuleName = @"<main>";

    explicit SourceHandler(NSString *Src, NSString *File = MainModuleName)
        : Base(new char16_t[Src.length + 1], Src.length + 1), File([File copy]) {

        unichar *chars = const_cast<unichar *>(Base.ptr<unichar>());
        [Src getCharacters:chars range:NSMakeRange(0, Base.size() - 1)];
        chars[Base.size() - 1] = '\n';
    }

    SourceHandler(SourceHandler &&Other) : Base(std::move(Other.Base)), File(std::move(Other.File)) {
        Other.File = nil;
    }

    ~SourceHandler() { delete[] Base.ptr(); }

    SourceRef source() const { return Base; }
    NSString *file() const { return [File copy]; }
};
}

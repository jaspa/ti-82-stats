//
//  DiagnosisHandler.h
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TI82Diagnosis.h"

@protocol DiagnosisHandler<NSObject>

- (void)handleDiagnosis:(TI82::Diagnosis)diagnosis;

@end

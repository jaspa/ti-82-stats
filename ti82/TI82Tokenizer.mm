//
//  TI82Tokenizer.mm
//  TI-82 STATS
//
//  Created by Janek Spaderna on 13.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82Tokenizer.h"

#if DEBUG

// Hilfsfunktionen für den Debugger
//
// Diese Funktionen sind zur Ausgabe des Names vom Tokens/Token-Gruppen im Debugger. Nur ohne `inline` und `static` ist
// garantiert, dass der Compiler diese Funktionen nicht wegoptimiert, da sie nie direkt aufgerufen werden.

const char *getGroupDebugName(TokGroup g) {
    return g.valid() ? g.name() : "<invalid>";
}

std::string getTokDebugName(Tok t) {
    return t.valid() ? t.fullName() : "<invalid>";
}

#endif

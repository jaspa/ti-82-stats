//
//  ColoredOstream.mm
//  ti82
//
//  Created by Janek Spaderna on 15.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColoredOstream.h"

bool ReferencingColoredOstream::xcodeColoring = []() {
    const char *colors = getenv("XcodeColors");
    return colors && strcmp(colors, "YES") == 0;
}();

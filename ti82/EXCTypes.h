//
//  EXCTypes.h
//  ti82
//
//  Created by Janek Spaderna on 05.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <llvm/ADT/ArrayRef.h>
#include <m_apm.h>

#import "TI82AST.h"

namespace EXC {
typedef NS_ENUM(uint8_t, Type) { TypeNull, TypeNumber, TypeList, TypeMatrix, TypeString, TypeDynamic };
NSString *TypeToString(Type t);
}

#pragma mark -

@protocol EXCTypedObject<NSObject>
@property (nonatomic, readonly) EXC::Type exc_type;
@end

@protocol EXCRuntimeValue<EXCTypedObject>
@property (nonatomic, readonly) NSString *exc_stringRepresentation;
@end

#pragma mark -

@interface ASTNode (EXCTypedObject) <EXCTypedObject>
@end

#pragma mark -

@interface EXCNumber : NSObject<EXCRuntimeValue>

+ (instancetype)makeNumber:(M_APM)raw;
+ (instancetype)borrowNumber:(M_APM)raw;
- (instancetype)initWithRawValue:(M_APM)raw freeWhenDone:(BOOL)free NS_DESIGNATED_INITIALIZER;

@property (nonatomic, readonly, assign) M_APM raw;

@end

// Used as lists (array of EXCNumber's)
@interface NSArray (EXCValue) <EXCRuntimeValue>
@end

@interface EXCString : NSObject<EXCRuntimeValue>

+ (instancetype)makeStringWithData:(NSString *)data indices:(llvm::ArrayRef<uint32_t>)indices;
- (instancetype)initWithData:(NSString *)data indices:(llvm::ArrayRef<uint32_t>)indices NS_DESIGNATED_INITIALIZER;

@property (nonatomic, readonly, copy) NSString *data;
@property (nonatomic, readonly, assign) llvm::ArrayRef<uint32_t> indices;

- (EXCString *)stringByAppendingString:(EXCString *)str;

@end

// Returned by nodes not changing Ans
@interface NSNull (EXCValue) <EXCRuntimeValue>
@end

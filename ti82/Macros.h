//
//  Macros.h
//  ti82
//
//  Created by Janek Spaderna on 25.01.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

#define __CON(x, y) x##y
#define CON(x, y) __CON(x, y)

//
//  ICUAdditions.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 13.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <unicode/utypes.h>

#ifndef NDEBUG
#define ASSERT_SUCCESS(E, desc, ...)                                                                            \
    do {                                                                                                        \
        UErrorCode __e = (E);                                                                                   \
        if (U_FAILURE(__e)) {                                                                                   \
            [[NSAssertionHandler currentHandler]                                                                \
                handleFailureInFunction:[NSString stringWithUTF8String:__PRETTY_FUNCTION__]                     \
                                   file:[NSString stringWithUTF8String:__FILE__]                                \
                             lineNumber:__LINE__                                                                \
                            description:@"%@ (%d: %s)", [NSString stringWithFormat:(desc), ##__VA_ARGS__], __e, \
                                        u_errorName(__e)];                                                      \
        }                                                                                                       \
    } while (0)
#else
#define ASSERT_SUCCESS(...)
#endif

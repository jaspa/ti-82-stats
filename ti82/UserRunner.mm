//
//  UserRunner.m
//  ti82
//
//  Created by Janek Spaderna on 02.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "UserRunner.h"

#include <llvm/Support/MemoryBuffer.h>
#include <memory>

#import "TI82SourceHandler.h"
#import "TI82Parser.h"
#import "ASTExecution.h"
#import "ExecRegistry.h"

#define TI82_Error(msg, ...)                                                                                          \
    do {                                                                                                              \
        using TI82::Diagnostic;                                                                                       \
        Diagnostic::send(context[DiagnosisHandlerKey], Diagnostic::Error, [=](Diagnostic & diag) {                    \
            diag.message = [NSString stringWithFormat:(msg), ##__VA_ARGS__];                                          \
            diag.errorRange.base = reinterpret_cast<TI82::SourceHandler *>([context[SourceHandlerKey] pointerValue]); \
        });                                                                                                           \
    } while (0)

@implementation UserRunner {
    std::unique_ptr<TI82::SourceHandler> source;
    NSUInteger errorCount;
    NSUInteger warningCount;
}

- (instancetype)initWithFile:(NSString *)file {
    if ((self = [super init])) {
        _file = [file copy];
    }
    return self;
}

- (void)createSource {
    if (self.file && ![self.file isEqualToString:@"-"]) {
        NSError *error;
        NSString *code = [NSString stringWithContentsOfFile:self.file encoding:NSUTF8StringEncoding error:&error];

        if (!code) {
            llvm::errs() << "fatal error: " << error.description.UTF8String << '\n';
            exit(EXIT_FAILURE);
        }

        source = std::make_unique<TI82::SourceHandler>(code, self.file);
    } else {
        auto buffer = llvm::MemoryBuffer::getSTDIN();

        if (auto error = buffer.getError()) {
            llvm::errs() << "fatal error: " << error.message() << '\n';
            exit(EXIT_FAILURE);
        }

        NSString *code = [[NSString alloc] initWithBytes:(*buffer)->getBufferStart()
                                                  length:(*buffer)->getBufferSize()
                                                encoding:NSUTF8StringEncoding];

        source = std::make_unique<TI82::SourceHandler>(code, _file = @"<stdin>");
    }
}

- (int)run {
    [self createSource];
    TI82::Parser parser(*source, self);

    NSMutableDictionary *variables = [NSMutableDictionary dictionary];
    NSDictionary *excContext = @{
        DiagnosisHandlerKey : self,
        SourceHandlerKey : [NSValue valueWithPointer:source.get()],
        VariablesDictionaryKey : variables,
        FunctionRegistryKey : [[ExecRegistry alloc] init]
    };

    while (parser) {
        ASTNode *node = parser.ParseNode();

        if (!node) {
            // TODO: try error recovery
            // parser.skipToken();
            // continue;
            break;
        }

        id<EXCRuntimeValue> value = [node executeInContext:excContext];

        if (value && value.exc_type != EXC::TypeNull) {
            variables[@"Ans"] = value;
        }
    }

    llvm::errs() << "Finished with " << warningCount << ' ' << (warningCount == 1 ? "warning" : "warnings") << " and "
                 << errorCount << ' ' << (errorCount == 1 ? "error" : "errors") << '\n';
    llvm::errs() << variables.description.UTF8String << '\n';

    return errorCount == 0 ? EXIT_SUCCESS : EXIT_FAILURE;
}

- (void)handleDiagnosis:(TI82::Diagnosis)diagnosis {
    if (diagnosis.kind == TI82::Diagnosis::Error)
        ++errorCount;

    if (diagnosis.kind == TI82::Diagnosis::Warning)
        ++warningCount;

    diagnosis.render(llvm::errs());
}

@end

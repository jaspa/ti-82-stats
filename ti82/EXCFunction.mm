//
//  EXCFunction.m
//  ti82
//
//  Created by Janek Spaderna on 11.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "EXCFunction.h"

#include <vector>

#import "ASTExecution.h"
#import "DiagnosisHandler.h"
#import "CTBlockDescription.h"

using namespace TI82;
using namespace EXC;
using namespace std;

NSString *const CallRangeKey = @"CallRangeKey";

NS_INLINE void DIAGNOSIS(NSDictionary *context,
                         Diagnosis::Kind kind,
                         std::function<NSArray *(NSDictionary *)> notesProvider,
                         NSString *message,
                         ...) {
    va_list args;
    va_start(args, message);
    message = [[NSString alloc] initWithFormat:message arguments:args];
    va_end(args);

    Diagnosis::send(context[DiagnosisHandlerKey], kind, [=, &notesProvider](Diagnosis &diag) {
        NSRange range = [context[CallRangeKey] rangeValue];

        diag.errorRange.range = SourceRef(reinterpret_cast<const char16_t *>(uintptr_t(range.location)), range.length);
        diag.errorRange.base = reinterpret_cast<TI82::SourceHandler *>([context[SourceHandlerKey] pointerValue]);
        diag.message = message;

        if (notesProvider)
            diag.notes = notesProvider(context);
    });
}

static id<EXCRuntimeValue> CallFunction(id function, NSArray *args, NSDictionary *context) {
#if !defined(NS_BLOCK_ASSERTIONS)
    NSMethodSignature *sig = [[CTBlockDescription alloc] initWithBlock:function].blockSignature;
    NSCAssert(sig.numberOfArguments - 2 == args.count, @"%lu vs %lu arguments", sig.numberOfArguments - 2, args.count);
#endif

    id<EXCRuntimeValue> (^block)(NSDictionary *, ...) = function;

    switch (args.count) {
        case 0: return block(context);
        case 1: return block(context, args.firstObject);
        case 2: return block(context, args.firstObject, args.lastObject);
        case 3: return block(context, args[0], args[1], args[2]);
        case 4: return block(context, args[0], args[1], args[2], args[3]);
        case 5: return block(context, args[0], args[1], args[2], args[3], args[4]);
        default: {
            NSLog(@"%s:%d:%s: %lu arguments not supported", __FILE__, __LINE__, __func__, args.count);
            abort();
        }
    }
}

NS_INLINE NSArray *StringifyOverloads(llvm::ArrayRef<FunctionOverload> overloads, NSDictionary *context) {
    NSMutableArray *strings = [NSMutableArray arrayWithCapacity:overloads.size()];
    auto output = back_inserter(strings);
    transform(overloads.begin(), overloads.end(), output,
              [](auto &ov) { return [@"possible overload: " stringByAppendingString:ov.first.toString()]; });
    return strings;
}

@implementation EXCFunction {
    vector<FunctionOverload> _overloads;
}

- (llvm::ArrayRef<FunctionOverload>)overloads {
    return _overloads;
}

#pragma mark - Initializers

+ (instancetype)functionWithOverloads:(llvm::ArrayRef<EXC::FunctionOverload>)overloads {
    return [[self alloc] initWithOverloads:overloads preevaluateArguments:YES];
}

+ (instancetype)macroWithOverloads:(llvm::ArrayRef<EXC::FunctionOverload>)overloads {
    return [[self alloc] initWithOverloads:overloads preevaluateArguments:NO];
}

- (instancetype)initWithOverloads:(llvm::ArrayRef<FunctionOverload>)overloads preevaluateArguments:(BOOL)preeval {
    if ((self = [super init])) {
        _overloads = overloads;
        _requiresEvaluatedArguments = preeval;
    }
    return self;
}

#pragma mark - Calling the function

- (id<EXCRuntimeValue>)callWithArguments:(NSArray *)args inContext:(NSDictionary *)context {
    NSUInteger count = args.count;

    NSIndexSet *possibleOverloads = [[NSMutableIndexSet indexSetWithIndexesInRange:{0, _overloads.size()}]
        indexesPassingTest:^BOOL(NSUInteger idx, BOOL *stop) {
          return _overloads[idx].first.argumentTypes().size() == count;
        }];

    if (possibleOverloads.count == 0) {
        DIAGNOSIS(context, Diagnosis::Error, bind(StringifyOverloads, _overloads, placeholders::_1),
                  @"no overload for %ld arguments found", count);
        return nil;
    }

    if (!self.requiresEvaluatedArguments) {
        if (possibleOverloads.count > 1) {
            size_t idx = 0;
            llvm::SmallVector<FunctionOverload, 4> overloadSpecs;

            copy_if(_overloads.begin(), _overloads.end(), back_inserter(overloadSpecs),
                    [=, &idx](const FunctionOverload &) { return [possibleOverloads containsIndex:idx++]; });

            DIAGNOSIS(context, Diagnosis::Error, bind(StringifyOverloads, overloadSpecs, placeholders::_1),
                      @"ambigous function call when not evaluating arguments");
            return nil;
        }

        return CallFunction(_overloads[possibleOverloads.firstIndex].second, args, context);
    }

    NSMutableArray *evaluatedArgs = [NSMutableArray arrayWithCapacity:count];

    for (ASTExpression *arg in args) {
        id<EXCTypedObject> value = [arg executeInContext:context];

        if (!value)
            return nil;

        NSCAssert(value.exc_type != TypeNull, @"invalid type: %@", value);

        possibleOverloads = [possibleOverloads indexesPassingTest:^BOOL(NSUInteger idx, BOOL *stop) {
          return _overloads[idx].first[evaluatedArgs.count].first == value.exc_type;
        }];

        [evaluatedArgs addObject:value];
    }

    if (possibleOverloads.count == 0) {
        llvm::SmallVector<QualifiedType, 5> argTys;

        for (id<EXCTypedObject> arg in evaluatedArgs) {
            argTys.push_back({arg.exc_type, false});
        }

        DIAGNOSIS(context, Diagnosis::Error, bind(StringifyOverloads, _overloads, placeholders::_1),
                  @"no matching overload for call %@", FunctionSignature(argTys).toString());
        return nil;
    }

    if (possibleOverloads.count > 1) {
        size_t idx = 0;
        llvm::SmallVector<FunctionOverload, 4> overloadSpecs;

        copy_if(_overloads.begin(), _overloads.end(), back_inserter(overloadSpecs),
                [=, &idx](const FunctionOverload &) { return [possibleOverloads containsIndex:idx++]; });

        DIAGNOSIS(context, Diagnosis::Error, bind(StringifyOverloads, overloadSpecs, placeholders::_1),
                  @"ambigous function call");
        return nil;
    }

    return CallFunction(_overloads[possibleOverloads.firstIndex].second, evaluatedArgs, context);
}

@end

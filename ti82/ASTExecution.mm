//
//  ASTExecution.m
//  ti82
//
//  Created by Janek Spaderna on 05.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "ASTExecution.h"

#import "DiagnosisHandler.h"
#import "EXCFunction.h"
#import "FunctionRegistry.h"

#define TI82_Error(error, ...)                                                                                        \
    do {                                                                                                              \
        TI82::Diagnosis::send(context[DiagnosisHandlerKey], Diagnosis::Error, [=](Diagnosis & diag) {                 \
            diag.errorRange.range = self.source;                                                                      \
            diag.errorRange.base = reinterpret_cast<TI82::SourceHandler *>([context[SourceHandlerKey] pointerValue]); \
            diag.message = [NSString stringWithFormat:(error), ##__VA_ARGS__];                                        \
        });                                                                                                           \
        return nil;                                                                                                   \
    } while (0)

#define TI82_Error2(src, error, ...)                                                                                  \
    do {                                                                                                              \
        TI82::Diagnosis::send(context[DiagnosisHandlerKey], Diagnosis::Error, [=](Diagnosis & diag) {                 \
            diag.errorRange.range = (src);                                                                            \
            diag.errorRange.base = reinterpret_cast<TI82::SourceHandler *>([context[SourceHandlerKey] pointerValue]); \
            diag.message = [NSString stringWithFormat:(error), ##__VA_ARGS__];                                        \
        });                                                                                                           \
        return nil;                                                                                                   \
    } while (0)

NSString *const DiagnosisHandlerKey = @"DiagnosisHandlerKey";
NSString *const VariablesDictionaryKey = @"VariablesDictionaryKey";
NSString *const SourceHandlerKey = @"SourceHandlerKey";
NSString *const FunctionRegistryKey = @"FunctionRegistryKey";

using namespace TI82;

#pragma mark Helpers

static void TypeError(SourceRef source, EXC::Type expected, EXC::Type found, NSDictionary *context) {
    Diagnosis::send(context[DiagnosisHandlerKey], Diagnosis::Error, [=](Diagnosis &diag) {
        diag.errorRange.range = source;
        diag.errorRange.base = reinterpret_cast<SourceHandler *>([context[SourceHandlerKey] pointerValue]);
        diag.message = [NSString
            stringWithFormat:@"invalid type %@ – expected %@", EXC::TypeToString(found), EXC::TypeToString(expected)];
    });
}

static BOOL RequireType(SourceRef source, id<EXCTypedObject> obj, EXC::Type type, NSDictionary *context) {
    if (obj.exc_type == type)
        return YES;

    TypeError(source, type, obj.exc_type, context);
    return NO;
}

static id<EXCRuntimeValue> ExecuteRequireType(ASTExpression *expr, EXC::Type type, NSDictionary *context) {
    if (id<EXCRuntimeValue> value = [expr executeInContext:context]) {
        if (value.exc_type == type)
            return value;

        TypeError(expr.source, type, value.exc_type, context);
    }

    return nil;
}

#pragma mark -
#pragma mark - Functions

@implementation ASTFunctionCall (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    EXCFunction *function = [context[FunctionRegistryKey] getFunction:self.functionID];

    if (!function) {
        TI82_Error(@"unimplemented function %s", self.functionID.name());
    }

    auto source = self.source;
    NSMutableDictionary *modifiedContext = [context mutableCopy];

    modifiedContext[CallRangeKey] =
        [NSValue valueWithRange:NSMakeRange(reinterpret_cast<uintptr_t>(source.ptr()), source.size())];

    return [function callWithArguments:self.arguments inContext:modifiedContext];
}

@end

#pragma mark - Literals

@implementation ASTNumberLiteral (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    auto num = m_apm_init();
    m_apm_copy(num, self.numValue);
    return [EXCNumber makeNumber:num];
}

@end

@implementation ASTListLiteral (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    NSArray *exprs = self.elements;
    NSMutableArray *list = [NSMutableArray arrayWithCapacity:exprs.count];

    for (ASTExpression *val in exprs) {
        if (id obj = ExecuteRequireType(val, EXC::TypeNumber, context)) {
            [list addObject:obj];
            continue;
        }

        return nil;
    }

    return list;
}

@end

@implementation ASTStringLiteral (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    return [EXCString makeStringWithData:self.content.getNSString(true) indices:self.userIndices];
}

@end

#pragma mark - Variables

@implementation ASTVarRef (Execution)

- (id<EXCRuntimeValue>)executionDefaultValueInContext:(NSDictionary *)context {
    TI82_Error(@"Undefined variable %@", self.name);
}

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    return context[VariablesDictionaryKey][self.name] ?: [self executionDefaultValueInContext:context];
}

@end

@implementation ASTNumberVarRef (Execution)

- (id<EXCRuntimeValue>)executionDefaultValueInContext:(NSDictionary *)context {
    return [EXCNumber new];
}

@end

#pragma mark -
#pragma mark - Statements

@implementation ASTBlockStatement (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    id last;

    for (ASTNode *node in self.nodes) {
        if (id value = [node executeInContext:context])
            last = value;
    }

    return last;
}

@end

@implementation ASTIfStatement (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    if (EXCNumber *cond = ExecuteRequireType(self.condition, EXC::TypeNumber, context))
        return [m_apm_sign(cond.raw) ? self.thenBlock : self.elseBlock executeInContext:context];
    return nil;
}

@end

@implementation ASTWhileLoop (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    NSMutableDictionary *variables = context[VariablesDictionaryKey];

    while (EXCNumber *cond = ExecuteRequireType(self.condition, EXC::TypeNumber, context)) {
        if (!m_apm_sign(cond.raw))
            return nil;

        if (id ans = [self.body executeInContext:context])
            variables[ASTAnsVarName] = ans;
    }

    return nil;
}

@end

@implementation ASTRepeatLoop (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    NSMutableDictionary *variables = context[VariablesDictionaryKey];

    while (true) {
        if (id ans = [self.body executeInContext:context])
            variables[ASTAnsVarName] = ans;

        if (EXCNumber *cond = ExecuteRequireType(self.condition, EXC::TypeNumber, context)) {
            if (!m_apm_sign(cond.raw))
                continue;
        }

        return nil;
    }
}

@end

@implementation ASTForLoop (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    EXCNumber *stepper = [self.stepper executeInContext:context];
    EXCNumber *init, *end;

    if (stepper) {
        if (!RequireType(self.stepper.source, stepper, EXC::TypeNumber, context))
            return nil;
        if (m_apm_sign(stepper.raw) == 0)
            TI82_Error2(self.stepper.source, @"the stepper value is 0");
    }

    if (!(init = ExecuteRequireType(self.start, EXC::TypeNumber, context)))
        return nil;
    if (!(end = ExecuteRequireType(self.end, EXC::TypeNumber, context)))
        return nil;

    NSMutableDictionary *variables = context[VariablesDictionaryKey];
    NSString *varName = self.variable.name;
    variables[varName] = init;

    M_APM stepVal = stepper.raw ?: MM_One;

    while (true) {
        EXCNumber *var = variables[varName];

        if (!var)
            TI82_Error(@"%@ is undefined", varName);

        if (m_apm_compare(var.raw, end.raw) >= 0)
            break;

        auto var2 = m_apm_init();
        m_apm_add(var2, var.raw, stepVal);

        variables[varName] = [EXCNumber makeNumber:var2];

        if (id ans = [self.body executeInContext:context])
            variables[ASTAnsVarName] = ans;
    }

    return nil;
}

@end

@implementation ASTDisplayStmt (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    for (ASTExpression *ex in self.expressions) {
        id<EXCRuntimeValue> eval = [ex executeInContext:context];
        llvm::outs() << eval.exc_stringRepresentation.UTF8String << '\n';
    }

    return nil;
}

@end

@implementation ASTStoreStmt (Execution)

- (id<EXCRuntimeValue>)executeInContext:(NSDictionary *)context {
    NSMutableDictionary *variables = context[VariablesDictionaryKey];
    id<EXCRuntimeValue> value = [self.expression executeInContext:context];

    if (!value)
        return nil;

    ASTVarRef *var = self.location;
    if (value.exc_type != var.exc_type) {
        TI82_Error(@"type mismatch: cannot store %@ into variable of type %@ (called %@)",
                   EXC::TypeToString(value.exc_type), EXC::TypeToString(var.exc_type), var.name);
    }

    variables[var.name] = value;
    return value;
}

@end

//
//  FunctionInfo.h
//  ti82
//
//  Created by Janek Spaderna on 19.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#import "TI82SourceRef.h"
#import "TokenTable.h"

#include <utility>

namespace AST {

typedef uint8_t precedence_t;

inline precedence_t lookupInfixFunction(Tok t) {
    if (t == Tok::op_power())
        return 100;
    if (t == Tok::op_nthRoot())
        return 70;
    if (t == Tok::op_multiply() || t == Tok::op_divide())
        return 50;
    if (t == Tok::op_plus() || t == Tok::op_minus())
        return 30;
    if (t.group() == TokGroup::RelationOp())
        return 10;
    if (t == Tok::op_and() || t == Tok::op_or() || t == Tok::op_xor())
        return 5;
    return 0;
}

inline precedence_t lookupPrefixFunction(Tok token) {
    return token == Tok::op_minus() ? 60 : 0;
}

inline bool checkPostfixFunction(Tok t) {
    return t == Tok::op_square() || t == Tok::op_cube() || t == Tok::op_reciprocal() || t == Tok::op_factorial();
}

inline bool checkCallFunction(Tok t) {
    return t == Tok::op_squareRoot() || t == Tok::op_cubicRoot() || t.group() == TokGroup::Function();
}
}

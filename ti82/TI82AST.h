//
//  TI82AST.h
//  TI-82 STATS
//
//  Created by Janek Spaderna on 16.07.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#include <llvm/ADT/ArrayRef.h>
#include <m_apm.h>

#import "TI82SourceRef.h"
#import "FunctionInfo.h"
#import "Types.h"

#pragma mark Node

@interface ASTNode : NSObject

+ (instancetype)nodeWithSource:(TI82::SourceRef)source;
- (instancetype)initWithSource:(TI82::SourceRef)source NS_DESIGNATED_INITIALIZER;

@property (nonatomic, readonly, assign) TI82::SourceRef source;
@property (nonatomic, readonly, assign) BOOL mayFormReference;

@end

#pragma mark - Expressions

@interface ASTExpression : ASTNode
@end

@interface ASTFunctionCall : ASTExpression

+ (instancetype)functionCallWithID:(Tok)funcID arguments:(NSArray *)args source:(TI82::SourceRef)source;

@property (nonatomic, readonly) Tok functionID;
@property (nonatomic, readonly, strong) NSArray *arguments;

@end

#pragma mark Literals

@interface ASTNumberLiteral : ASTExpression

+ (instancetype)numberLiteralWithValue:(M_APM)val source:(TI82::SourceRef)source;

@property (nonatomic, readonly, assign) M_APM numValue;

@end

@interface ASTListLiteral : ASTExpression

+ (instancetype)listLiteralWithElements:(NSArray *)elems source:(TI82::SourceRef)source;

@property (nonatomic, readonly) NSArray *elements;

@end

@interface ASTStringLiteral : ASTExpression

+ (instancetype)stringLiteralWithUserIndices:(llvm::ArrayRef<uint32_t>)indices source:(TI82::SourceRef)source;

@property (nonatomic, readonly) TI82::SourceRef content;
@property (nonatomic, readonly) llvm::ArrayRef<uint32_t> userIndices;

@end

#pragma mark Variables

@interface ASTVarRef : ASTExpression

+ (instancetype)varRefWithName:(TI82::SourceRef)name;

@property (nonatomic, readonly, strong) NSString *name;

@end

@interface ASTNumberVarRef : ASTVarRef
@end

@interface ASTListVarRef : ASTVarRef
@end

@interface ASTStringVarRef : ASTVarRef
@end

@interface ASTAnsVarRef : ASTVarRef
@end

extern NSString *const ASTAnsVarName;

#pragma mark - Statements

@interface ASTStatement : ASTNode
@end

@interface ASTBlockStatement : ASTStatement

+ (instancetype)blockWithNodes:(NSArray *)nodes source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) NSArray *nodes;

@end

@interface ASTIfStatement : ASTStatement

+ (instancetype) if:(ASTExpression *)condition
               then:(ASTBlockStatement *)thenBlock
               else:(ASTBlockStatement *)elseBlock
             source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) ASTExpression *condition;
@property (nonatomic, readonly, strong) ASTBlockStatement *thenBlock;
@property (nonatomic, readonly, strong) ASTBlockStatement *elseBlock;

@end

@interface ASTLoopStatement : ASTStatement

- (instancetype)initWithBody:(ASTBlockStatement *)body source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) ASTBlockStatement *body;

@end

@interface ASTConditionedLoopStatement : ASTLoopStatement

+ (instancetype)loopOverBlock:(ASTBlockStatement *)body
                    condition:(ASTExpression *)condition
                       source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) ASTExpression *condition;

@end

@interface ASTWhileLoop : ASTConditionedLoopStatement
@end

@interface ASTRepeatLoop : ASTConditionedLoopStatement
@end

@interface ASTForLoop : ASTLoopStatement

+ (instancetype)loopFor:(ASTNumberVarRef *)var
                   from:(ASTExpression *)start
                     to:(ASTExpression *)end
                     by:(ASTExpression *)stepper
                     do:(ASTBlockStatement *)body
                 source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) ASTNumberVarRef *variable;
@property (nonatomic, readonly, strong) ASTExpression *start, *end, *stepper;

@end

@interface ASTDisplayStmt : ASTStatement

+ (instancetype)displayExpressions:(NSArray *)exprs source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) NSArray *expressions;

@end

@interface ASTProgramCall : ASTStatement

+ (instancetype)programmCallWithSource:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) NSString *name;

@end

@interface ASTStoreStmt : ASTStatement

+ (instancetype)storeExpression:(ASTExpression *)expr intoVariable:(ASTVarRef *)var source:(TI82::SourceRef)source;

@property (nonatomic, readonly, strong) ASTExpression *expression;
@property (nonatomic, readonly, strong) ASTVarRef *location;

@end

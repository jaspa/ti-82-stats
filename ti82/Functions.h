//
//  Functions.h
//  ti82
//
//  Created by Janek Spaderna on 04.11.14.
//  Copyright (c) 2014 Janek Spaderna. All rights reserved.
//

#include <llvm/ADT/Optional.h>
#include <iterator>
#include <type_traits>

template <class T, class F, class = std::enable_if_t<std::is_void<std::result_of_t<F(T)>>::value>>
bool operator>>(llvm::Optional<T> &&opt, F &&f) {
    return opt ? f(*std::move(opt)), true : false;
}

template <class T, class F, class = std::enable_if_t<!std::is_void<std::result_of_t<F(T)>>::value>>
llvm::Optional<std::result_of_t<F(T)>> operator>>(llvm::Optional<T> &&opt, F &&f) {
    return opt ? f(*std::move(opt)) : llvm::Optional<std::result_of_t<F(T)>>();
}

template <class T, class F, class _R = std::result_of_t<F()>, class = std::enable_if_t<std::is_same<_R, T>::value>>
T operator|(llvm::Optional<T> &&opt, F &&f) {
    return opt ? *std::move(opt) : f();
}

template <class T>
T operator|(llvm::Optional<T> &&opt, T &&v) {
    return std::move(opt).getValueOr(std::forward<T>(v));
}

template <class It>
std::reverse_iterator<It> make_reverse_iterator(It it) {
    return std::reverse_iterator<It>(it);
}
